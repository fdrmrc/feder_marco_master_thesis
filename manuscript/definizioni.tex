

\section{The Laplace operator}
\begin{definition}[The Laplace operator and its discretization]
The main differential operator that we will consider is the Laplacian $\Delta \colon L^2 (\Omega) \rightarrow L^2(\Omega)$, with $\Omega \subset \mathbb{R}^n$ open and bounded. This operator is unbounded, indeed if we take $\Omega = (0,1)$ and define $\varphi_k(x)=\sin(k \pi x)$ we have:
\[ 
\norm{\Delta \varphi_k(x)}_{L^2} = \norm{- \sin(k \pi x)k ^2 \pi^2}_{L^2} =  \frac{k^2 \pi^2}{2}
\]
and as $k \rightarrow \infty$ the norm explodes. This is reflected also in its classical finite difference discretization: let $A_h$ be the tridiagonal, symmetric matrix stemming from the stencil $[1 \quad-2 \quad 1 ]$ with homogeneos Dirichlet boundary conditions and step size $h$. It is 


\begin{equation}
\label{mat:lapl}
A_h = \frac{1}{h^2}
\begin{bmatrix}
-2 & 1 & 0 &  0& \ldots &0 \\ 
1 & -2 & 1 & 0 &\ldots & 0 \\ 
0 & 1 & -2 &1 	&  \ldots & 0\\
\vdots & \ddots & \ddots & \ddots & \ddots  & \vdots \\
0& \ldots& \ldots&1&-2&1 \\
0&\ldots &\ldots &0&1&-2
\end{bmatrix}
\end{equation}

 Even if its norm is finite, we have $\norm{A_h}_i \geq \frac{K_i}{h^2}$, where $K_i$ depends on the norm ($K_2 \approx 3.7, K_1=K_{\infty}=4$),  and hence the norm increases as we refine the grid. Finally, to compute its eigenvectors and eigenvalues, let $x_l=lh$ and define $\varphi_{k,h} = \bigl( \sin(k \pi x_l) \bigr)_{l=1}^n$. Then we have 
\[ 
%(A_h \varphi_{k,h} \bigr)_l = \frac{1}{h^2} \bigl(\sin(k \pi h(l-1)) - 2 \sin(k \pi l h) + \sin(k \pi h (l+1)) \bigr) = \frac{2}{h^2} \bigl( \cos(k \pi h) - 1 \bigr) \varphi_{k,h}
A_h \varphi_{k,h} = \frac{1}{h^2} \bigl(\sin(k \pi h(l-1)) - 2 \sin(k \pi l h) + \sin(k \pi h (l+1)) \bigr)_{l=1}^n = \frac{2}{h^2} \bigl( \cos(k \pi h) - 1 \bigr) \varphi_{k,h}
\]

So the eigenvalues of the operator are real and lie in the left half plane, that is $\sigma(A_h) \subset [- \norm{A_h}_2,0]$.
\end{definition}



For periodic boundary conditions $u(0)=u(1)$, the resulting symmetric matrix is 

\begin{equation}
\label{mat:laplperiodic}
A_h = \frac{1}{h^2}
\begin{bmatrix}
-2 & 1 & 0 &  0& \ldots &1 \\ 
1 & -2 & 1 & 0 &\ldots & 0 \\ 
0 & 1 & -2 &1 	&  \ldots & 0\\
\vdots & \ddots & \ddots & \ddots & \ddots  & \vdots \\
0& \ldots& \ldots&1&-2&1 \\
1&\ldots &\ldots &0&1&-2
\end{bmatrix}
\end{equation}


\section{Properties of Kronecker product $\otimes$}
\begin{definition}[Kronecker product]
Let $A \in \mathbb{C}^{m \times n}$ and $B \in \mathbb{C}^{p \times q}$. The Kronecker product is defined as 
\[
A \otimes B = 
\begin{bmatrix}
a_{11}B & \ldots & a_{1n}B \\
\vdots & \ddots & \vdots \\
a_{m1} B & \ldots & a_{mn} B
\end{bmatrix}
\in \mathbb{C}^{mp \times nq}
\]

\end{definition}


\begin{definition}[Kronecker form of a matrix]
\label{def:kronmatrx}
A matrix $M \in \mathbb{C}^{n_1 n_2 \ldots n_d \times n_1 n_2 \ldots n_d}$ is in Kronecker form if it's such that
\[
M = \sum_{\mu=1}^d I_d \otimes \ldots \otimes I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu-1} \otimes \ldots \otimes I_1
\]
where $I_{\mu}$ is the identity matrix of size $n_{\mu}$ and $A_{\mu}$ a complex square matrix of size $n_{\mu}$.

An important example of a matrix in Kronecker form is the two dimensional discretization of the Laplacian, which can be written as 

\[
M = I_2 \otimes A_1 + A_2 \otimes I_1
\]
where each $A_i$ is \eqref{mat:lapl}. This construction can be readily generalized to the $d$-dimensional case.
\end{definition}

\begin{definition}[Vec operator]
\label{def:vecoper}
Let $A \in \mathbb{C}^{m \times n}$. The $\operatorname{vec}$ is the \emph{vector} obtained by stacking by columns all the elements of the matrix $A$. In MatLab language it can be computed with \verb|A(:)|.
\end{definition}


\begin{definition}[Properties of the Kronecker product]
The Kronecker product satisfies some basic properties (see \cite{Kron} for details) :
\begin{itemize}
\label{def:prop}
\item $A \otimes (B_1 + B_2) = A \otimes B_1 + A \otimes B_2$ for $A \in \mathbb{C}^{m \times n}$ and $B_1, B_2 \in \mathbb{C}^{p \times q}$;
\item $(B_1 + B_2) \otimes A = B_1 \otimes A + B_2 \otimes A$ for $B_1,B_2 \in \mathbb{C}^{p \times q}$ and $A \in \mathbb{C}^{m \times n}$;
\item $(\lambda A) \otimes B =A \otimes (\lambda B) = \lambda (A \otimes B)$ for $\lambda \in \mathbb{C}, A \in \mathbb{C}^{m \times n}$ and $B \in \mathbb{C}^{p \times q}$;
\item $(A \otimes B)\otimes C = A \otimes (B \otimes C)$ for $A \in \mathbb{C}^{m \times n}$, $B \in \mathbb{C}^{p \times q}$ and $C \in \mathbb{C}^{r \times s}$;
\item \emph{Mixed product property}: $(A \otimes B)(D \otimes E)= (AD) \otimes (BE)$ for $A \in \mathbb{C}^{m \times n}$, $B \in \mathbb{C}^{p \times q}$, $D \in \mathbb{C}^{n \times r}$ and $C \in \mathbb{C}^{q \times s}$;
\item $\operatorname{vec}(ADC) = (C^T \otimes A) \operatorname{vec}(D)$ for $A \in \mathbb{C}^{m \times n}$, $D \in \mathbb{C}^{n \times r}$ and $C \in \mathbb{C}^{r \times s}$.
\end{itemize}
\end{definition}

Let $\boldsymbol{B} \in \mathbb{C}^{n_1 \times n_2 \times \ldots \times n_d}$ be a $d$-dimensional tensor.
\begin{definition}[$\mu$-fiber of a tensor]
A $\mu$-fiber of a tensor is a \emph{vector} in $\mathbb{C}^{n_{\mu}}$ obtained by fixing every index of the tensor but the $\mu-$th.
\end{definition}
For a $1$-dimensional tensor, the only fiber is the vector itself. For a $2$-dimensional tensor, the $1$-fiber are the columns and the $2-$fiber are the rows. In the $3$- dimensional case, we can consider an explicit example like the following $\boldsymbol{B} \in \mathbb{C}^{2 \times 2 \times 2}$:

\[
\begin{bmatrix}
b_{111} & b_{121} \\
b_{211} & b_{221}
\end{bmatrix}
 = \begin{bmatrix}
1 & 3  \\
2 & 4
\end{bmatrix}
,
\begin{bmatrix}
b_{112} & b_{122} \\
b_{212} & b_{222}
\end{bmatrix}
 = \begin{bmatrix}
5 & 7  \\
6 & 8
\end{bmatrix}
\]

The $1$-,$2$,$3$- fibers are given by 

\[
\mathcal{F}_1 = \Bigl\{  \begin{bmatrix} 1 \\ 2 \end{bmatrix}, \begin{bmatrix} 3 \\ 4 \end{bmatrix}, \begin{bmatrix} 5 \\ 6 \end{bmatrix}, \begin{bmatrix} 7 \\ 8 \end{bmatrix} \Bigr\}
\]
\[
\mathcal{F}_2 = \Bigl\{  \begin{bmatrix} 1 \\ 3 \end{bmatrix}, \begin{bmatrix} 2 \\ 4 \end{bmatrix}, \begin{bmatrix} 5 \\ 7 \end{bmatrix}, \begin{bmatrix} 6 \\ 8 \end{bmatrix} \Bigr\}
\]
\[
\mathcal{F}_3 = \Bigl\{  \begin{bmatrix} 1 \\ 5 \end{bmatrix}, \begin{bmatrix} 2 \\ 6 \end{bmatrix}, \begin{bmatrix} 3 \\ 7 \end{bmatrix}, \begin{bmatrix} 4 \\ 8 \end{bmatrix} \Bigr\}
\]

A graphical representation of fibers in the $3$-dimensional case is the following

\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{fibers3d.png}
\caption[Fibers representation]{Fibers for a $3$-d tensor $\boldsymbol{x}$.}
\label{fig:fibers}
\end{figure}
\newpage
Thanks to the notion of fibers we can define the $\mu$-matricization of a tensor $\boldsymbol{B}$.

\begin{definition}[$\mu$-matricization of a tensor]
The $\mu$-matricization of a tensor  $\boldsymbol{B}$, denoted by $B^{(\mu)} \in \mathbb{C}^{n_{\mu} \times  n_1 \ldots n_{\mu-1} n_{\mu+1} \ldots n_d}$ is defined as the \emph{matrix} whose columns are the $\mu$-fibers of $\boldsymbol{B}$, considered moving along the different directions of $\boldsymbol{B}$ in increasing order.
\end{definition}

Applying this concept to the tensor $\boldsymbol{B}$ of the previous example, we obtain the $1$-,$2$-,$3$-matricizations

\[
B^{(1)} = \begin{bmatrix} 1 & 3 & 5 & 7 \\ 2 & 4 & 6 & 8 \end{bmatrix} 
\]

\[
B^{(2)} = \begin{bmatrix} 1 & 2 & 5 & 6 \\ 3 & 4 & 7 & 8 \end{bmatrix} 
\]

\[
B^{(3)} = \begin{bmatrix} 1 & 2 & 3 & 4 \\ 5 & 6 & 7 & 8 \end{bmatrix} 
\]

A graphical representation is the following

\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{1matriciz.png}
\caption[$\mu$-matricization]{$1$-matricization for a $3$-d tensor.}
\label{fig:fibers}
\end{figure}

It may be easier to understand the matricization using MatLab notation. Given a $3$-d tensor \verb|B| and \verb|n = size(B)|, we can compute the different matricizations using the following anonymous functions:

\verb|m1mat = @(B) reshape(B,n(1),[])| 
\newline
\verb|m2mat = @(B) reshape(permute(B,[2,1,3]),n(2),[])|
\newline
\verb|m3mat = @(B) reshape(permute(B,[3,1,2]),n(3),[])|
\newline

In MatLab, the command \verb|C = reshape(B,sz1,...,szN)| reshapes the $N$-dimensional array \verb|B| into a \verb|sz1|-by-$\ldots$-by-\verb|szN| array. If one specifies one dimension size to \verb|[]|, then the dimension is automatically computed in such a way that the number of elements in \verb|B| and \verb|C| is the same. Note indeed that the first dimension argument for the three matricizations is $n_{\mu}$, $\mu = 1, 2, 3$, while the second one (corresponding to \verb|[]|) is indeed $n_1 \ldots n_{\mu -1} n_{\mu+1} \ldots n_d$. The command \verb|C = permute(B,dimorder)| rearranges the dimension of the array \verb|B| in the order specified by \verb|dimorder|. 
\newline
We are now ready to define the concept of multiplication of a tensor with a matrix.
\begin{definition}[$\mu$-mode tensor product]
Let $A \in \mathbb{C}^{m \times n_{\mu}}$ be a matrix. Then the $\mu$-mode tensor-matrix multiplication of $\boldsymbol{B}$ with $A$, denoted by $\boldsymbol{B} \times_{\mu} A$, is the \emph{tensor} $\boldsymbol{Y} \in \mathbb{C}^{n_1 \times \ldots \times n_{\mu-1} \times m \times n_{\mu+1} \times  \ldots \times n_d}$  such that $Y^{(\mu)} = A B^{(\mu)}$, i.e.

\begin{equation}
\label{eqn:mumodetm}
y_{i_1 \ldots i_{\mu-1} i i_{\mu+1} \ldots i_d} = \sum_{j=1}^{n_{\mu}} x_{i_1 \ldots i_{\mu-1} j i_{\mu+1} \ldots i_d } a_{ij}, \quad i = 1,2,\ldots,m
\end{equation}
\end{definition}

In equation \eqref{eqn:mumodetm} we are multiplying from the left every $\mu$-fiber of the tensor $\boldsymbol{B}$ with the matrix $A$. To understand how this product works, consider the following examples in the $2$-dimensional case:

\begin{itemize}
\item The $1$-mode product is the matrix $\boldsymbol{Y}$ given by $A \boldsymbol{B}$ ($\boldsymbol{B}$ is a matrix). 
\item The $2$-mode product is the \emph{matrix} $\boldsymbol{Y}$ s.t. $$ Y^{(2)} = A B^{(2)} = A  \boldsymbol{B}^T$$
Since $ Y^{(2)} =  \boldsymbol{Y}^{T}$, it follows that $ \boldsymbol{Y}^T= A  \boldsymbol{B}^T \Rightarrow \boldsymbol{Y} = (A  \boldsymbol{B}^T )^T  =  \boldsymbol{B} A^T$
\end{itemize}

In the $3$-dimensional case we have to:
\begin{itemize}
\item Perform the $\mu$-matricization 
\item  Multiply from the left with the matrix $A$
\item  ``Tensorize'' the resulting matrix by reverting the $\mu$-matricization
\end{itemize}

This can be performed in MatLab as follows: given \verb|m = size(A,1)|, we have:

\verb| m1tm = @(B,A) reshape(A*m1mat(B),[m,n(2),n(3)])|
\newline
\verb| m2tm = @(B,A) ipermute(reshape(A*m2mat(B),[m,n(1),n(3)]),[2,1,3])|
\newline
\verb| m3tm = @(B,A) ipermute(reshape(A*m3mat(B)),[m,n(1),n(2)],[3,1,2])|
\newline

It's also clear from the definition that $\boldsymbol{B} \times_{\mu} I_{\mu} = \boldsymbol{B}$, because it holds that $Y^{(\mu)} = I_{\mu} B^{(\mu)} = B^{(\mu)}$.

\begin{definition}[Properties of the $\mu$-mode product]
The Kronecker product satisfies some properties (see \cite{KronKolda} for details) :
\begin{itemize}
\label{def:propmumo}
\item  $\boldsymbol{B}  \times_{\mu} A_1 \times_{\mu} A_2 = \boldsymbol{B} \times_{\mu} (A_2 A_1)$ for $\boldsymbol{B} \in \mathbb{C}^{n_1 \times \ldots \times n_d}$, $A \in \mathbb{C}^{m_1 \times n_{\mu}}$ and $A_2 \in \mathbb{C}^{m_2 \times m_1}$;
\item $\boldsymbol{B} \times_{\mu} A_1 \times_{\nu} A_2 = \boldsymbol{B} \times_{\nu} A_2 \times_{\mu} A_1$ for $\nu \ne \mu$, $\boldsymbol{B} \in \mathbb{C}^{n_1 \times \ldots \times n_d}$;
\item  $\boldsymbol{C} = \boldsymbol{B} \times_1 A_1 \times_2 \ldots \times_d A_d \iff C^{(\mu)} = A_{\mu}B^{(\mu)} (A_d \otimes \ldots \otimes A_{\mu+1} \otimes A_{\mu-1} \otimes \ldots \otimes A_1)^T$ for $\boldsymbol{B} \in \mathbb{C}^{n_1 \times \ldots \times n_d}$ and $A_{\mu} \in \mathbb{C}^{m_{\mu} \times n_{\mu}}$.
\end{itemize}
\end{definition}
