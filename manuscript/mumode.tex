
This new approach has been recently exploited in \autocite{mumode}. The idea is to use the special structure of the matrix $$M = \sum_{\mu=1}^d I_d \otimes \ldots \otimes I_{\mu +1} \otimes A_{\mu}  \otimes I_{\mu -1} \ldots \otimes I_1$$ defined in the first chapter.



\begin{lemma}
\label{thm:lemma_mumode}
Let $A_{\mu} \in \mathbb{C}^{n_{\mu} \times n_{\mu}}$. Then

\[
\Bigl(I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_\mu} \otimes I_{\mu-1} \otimes I_1 \Bigr)^k = I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_\mu}^k \otimes I_{\mu-1} \otimes \ldots \otimes I_1
\]

\end{lemma}

\begin{proof}

For $d=1$ it's trivial. Otherwise, using the \emph{mixed product property} (see \eqref{def:prop}) we have

\[
\Bigl( I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_\mu} \otimes I_{\mu-1} \otimes \ldots \otimes  I_1 \Bigr)^k = 
\prod_{j=1}^k \Bigl(   I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_\mu} \otimes I_{\mu-1} \otimes \ldots \otimes  I_1 \Bigr) =
\]
\[
 I_d \otimes \ldots \otimes I_{\mu+1} \otimes \prod_{j=1}^k A_{\mu} \otimes I_{\mu-1} \otimes I_1 =  I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_\mu}^k \otimes I_{\mu-1} \otimes \ldots   \otimes I_1
\]


\end{proof}


Using the previous  lemma, we can prove  the next

\begin{lemma}
\label{thm:lemma_mumode2}
Let $A_{\mu} \in \mathbb{C}^{n_{\mu} \times n_{\mu}}$. Then

\[
\exp(I_d \otimes \ldots I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu-1}\ldots \otimes I_1)=I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_\mu} \otimes I_{\mu-1} \otimes \ldots \otimes  I_1 
\]

\end{lemma}

\begin{proof}

For $d=1$, there's nothing to prove. For $d>1$, by using the definition of matrix exponential and the previous lemma:

\[
\exp(I_d \otimes \ldots I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu-1}\ldots \otimes I_1) = 
\sum_{k=0}^{\infty} \frac{\Bigl(I_d \otimes \ldots \otimes I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu-1} \otimes I_1 \Bigr)^k}{k!}\]
\[=\sum_{k=0}^{\infty} \frac{I_d \otimes \ldots \otimes I_{\mu+1} \otimes A_{\mu}^k \otimes I_{\mu-1} \otimes I_1}{k!} 
= \sum_{k=0}^{\infty} I_d \otimes \ldots \otimes I_{\mu+1} \otimes  \frac{A_{\mu}^k}{k!} \otimes I_{\mu-1} \otimes I_1=
\]
\[ 
I_d \otimes \ldots \otimes I_{\mu+1} \otimes \exp{A_{\mu}} \otimes I_{\mu-1} \otimes I_1
\]


\end{proof}

We can now state the main result of this chapter.

\begin{theorem}
\label{thm:thm_mumode}
Let $M$ a matrix in Kronecker form. Then

\begin{equation}
\label{eqn:expmumode}
\exp(M) = \exp(A_d) \otimes \ldots \otimes \exp(A_1)
\end{equation}

\end{theorem}

\begin{proof}

An explicit computation yields:

\[
\exp(M) = \exp  \Bigl(  \sum_{\mu = 1}^{d}  I_d \otimes \ldots \otimes I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu-1} \otimes \ldots \otimes I_1 \Bigr) = \prod_{\mu=1}^d \exp(I_d \otimes \ldots I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu-1}\ldots \otimes I_1)\]
\[ =_{\eqref{thm:lemma_mumode2}}  \prod_{\mu=1}^d I_d \otimes \ldots I_{\mu+1} \otimes \exp(A_{\mu}) \otimes I_{\mu-1}\ldots \otimes I_1 = \exp(A_d) \otimes \ldots \otimes \exp(A_1)
\]
\end{proof}

Given a vector $v$, formula \eqref{eqn:expmumode} yields 

\begin{equation}
\label{eqn:expkron}
\exp(M) v = \Bigl(\exp(A_d) \otimes \ldots \otimes \exp(A_1) \Bigr)v
\end{equation}
which is of no practical usage, because even if every $\exp(A_i)$ is cheap to compute, it involves $d-1$ Kronecker products which lead to a large full matrix (since $\exp(A_i)$ is full in general) that has to be applied to the vector $v$. So, in order to compute in a fast way $e^M v$, the following result is crucial


\begin{lemma}
\label{lemma:lemma_mumode3}
Let $\boldsymbol{B} \in \mathbb{C}^{n_1 \times \ldots \times n_d}$ a $d$-dimensional tensor and $A_{\mu} \in \mathbb{C}^{m_{\mu} \times n_{\mu}}$ be matrices. Then

\[
\boldsymbol{C} = \boldsymbol{B} \times_1 A_1 \times_2 \ldots \times_d A_d \iff \operatorname{vec}(\boldsymbol{C}) = \Bigl(  A_d \otimes \ldots \otimes A_1\Bigr) \operatorname{vec}(\boldsymbol{B})
\]
where the $\operatorname{vec}$ operator has been defined in \eqref{def:vecoper}
\end{lemma}

\begin{proof}
Using the third property of the $\mu$-mode product with $\mu=1$ we obtain 

\[
\boldsymbol{C} = \boldsymbol{B} \times_1 A_1 \times_2 \ldots \times_d A_d \iff C^{(1)} = A_1 B^{(1)} \Bigl(A_d \otimes \ldots \otimes A_2 \Bigr)^{T} \iff  \operatorname{vec}(C^{(1)}) = \Bigl(  A_d   \otimes \ldots \otimes A_2 \otimes A_1 \Bigr) \cdot \] $\cdot \operatorname{vec}(B^{(1)})$


Since $\operatorname{vec}(C^{(1)}) = \operatorname{vec}(\boldsymbol{C})$ we obtain 
\[
\boldsymbol{C} = \boldsymbol{B} \times_1 A_1 \times_2 \ldots \times_d A_d  \iff \operatorname{vec}(C) = \Bigl(  A_d \otimes \ldots \otimes A_1 \Bigr) \operatorname{vec}(\boldsymbol{B})
\]

\end{proof}




If we consider now 

\[
\boldsymbol{C} = \boldsymbol{B} \times_1 I_1 \times_2 \ldots \times_{\mu-1} I_{\mu-1} \times_{\mu} A_{\mu}  \times_{\mu+1}  I_{\mu +1} \times_{\mu+2} \ldots \times_{d} I_d
\]

by using the Lemma \eqref{lemma:lemma_mumode3} we obtain 
\[
\operatorname{vec}(\boldsymbol{C}) = (I_d \otimes \ldots \otimes I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu -1} \otimes \ldots \otimes I_1) \operatorname{vec}(\boldsymbol{B}) 
\]

Since for every $\mu$ it holds that $\boldsymbol{B} \times_{\mu} I_{\mu} =  \boldsymbol{B}$ we have that $\boldsymbol{C} = \boldsymbol{B} \times_{\mu} A_{\mu}$ and hence 

\[ 
\operatorname{vec} (\boldsymbol{B} \times_{\mu} A_{\mu}) = (I_d \otimes \ldots \otimes I_{\mu+1} \otimes A_{\mu} \otimes I_{\mu -1} \otimes \ldots \otimes I_1) \operatorname{vec}(\boldsymbol{B}) 
\]


In particular, the action of the matrix $M$ can be computed as

\[ 
M \operatorname{vec}(\boldsymbol{B}) = \sum_{\mu=1}^d \operatorname{vec} (\boldsymbol{B} \times_{\mu} A_{\mu}) 
\]


\subsection{Computing the action $e^M v$}

Let $E_i= \exp(A_i)$. Applying Lemma \eqref{lemma:lemma_mumode3} to \eqref{eqn:expkron} we obtain 

\begin{equation}
\label{eqn:expmumode}
\exp(M) v = \Bigl(  \exp(A_d) \otimes \ldots \otimes \exp(A_1) \Bigr) v\iff \boldsymbol{X} = \boldsymbol{B} \times_1 E_1 \times_2 \ldots \times_d E_d
\end{equation}

where $\boldsymbol{X}$ and $\boldsymbol{B}$ are $d$-dimensional tensors such that  $\operatorname{vec}(\boldsymbol{X}) = \exp(M)v$ and $\operatorname{vec}(\boldsymbol{B}) = v$. The formula \eqref{eqn:expmumode} is really convenient because it involves neither the explicit construction of the \emph{whole} matrix $M$ nor any Kronecker product. We just have to compute $d$ small matrix exponentials $E_i$, which can be done with $\verb|expm|$ routine.

\section{Numerical examples}

Consider the following matrix in Kronecker form
%the finite difference discretization of the Laplacian in the unit cube $[0,1]^3$, i.e. 
\[
 M =I \otimes I \otimes A_1 +  I \otimes A_2 \otimes I + A_3 \otimes I \otimes I
\]

where each $A_i$ is a random matrix with normally distributed components and with size $m=20$. Therefore the size of $M$ is $m^3 = 8000$. Consider now a random normally distributed compatible vector $v$. The pseudocode to compute $e^M v$ with the $\mu$-mode method (in the general case where each $A_i$ has a different size) is the following:

\begin{algorithm}
\caption{$\mu-$mode to compute $e^M v$}
\begin{algorithmic}
\State Store small matrices $A_1$, $A_2$, $A_3$.
\State Store compatible vector $v$ as tensor
\State Let $\boldsymbol{V}$ such that $\operatorname{vec}(\boldsymbol{V}) = v$
\State Precompute $E_1 = \exp(A_1)$, $E_2 = \exp(A_2)$, $E_3 = \exp(A_3)$.
\State $\boldsymbol{X} \gets \boldsymbol{V} \times_1 E_1 \times_2 \ldots \times_d E_d $
\State $e^{M} v  \gets \operatorname{vec}(\boldsymbol{X})$
\end{algorithmic}
\end{algorithm}

which in MatLab is the following

\begin{algorithm}
\caption{MatLab $\mu-$mode to compute $e^M v$}
\begin{algorithmic}
\State function X = mumod(E,v)
\State \%
\State \%INPUT: 
\State \%	-E: cell of matrices
\State \%	-v: compatible vector
\State \%OUTPUT:
\State \%	-action $e^M v$
\State m = size(E\{1\},1);
\State V= reshape(v,[m,m,m]);
\State n = size(V); 
\State m1mat = @(B) reshape(B,n(1),[]);
\State m2mat = @(B) reshape(permute(B,[2,1,3]),n(2),[]);
\State m3mat = @(B) reshape(permute(B,[3,1,2]),n(3),[]);

\State m1tm = @(B,A) reshape(A*m1mat(B),[m,n(2),n(3)]);
\State m2tm = @(B,A) ipermute(reshape(A*m2mat(B),[m,n(1),n(3)]),[2,1,3]);
\State m3tm = @(B,A) ipermute(reshape(A*m3mat(B),[m,n(1),n(2)]),[3,1,2]);
\State X = m3tm(m2tm(m1tm(V,E\{1\}),E\{2\}),E\{3\});
\State X = X(:); \%vec operator
\State end
\State m = 20; A = randn(m);
\State E\{1\} = expm(A);
\State E\{2\} = expm(A); \% in principle A could have different sizes
\State E\{3\} = expm(A);
\State action = mumod(E, randn(m*m*m,1));
\end{algorithmic}
\end{algorithm}


Measuring the computational times with \verb|tic-toc| on an Intel Core i5-6200U
CPU with 4GB of RAM, the output is the following: \verb|Elapsed time is 0.00357199 seconds|. The time to compute the $E_i$ has not been taken into account. 

\subsection{An application: Schrödinger equation}

Consider the following equation 
\begin{equation}
\label{eqn:schroed}
\begin{cases}
\psi_t(x,t)=H(x,t) \psi(x,t), \quad x \in [-10,10]^d \\
\psi(x,0)=e^{- \sqrt{\nu} \sum_{i=1}^d \frac{x_i^2}{2} }
\end{cases}
\end{equation}

with potential $H(x,t)=i \Bigl( \frac{1}{2} \Delta - \nu \sum_{i=1}^{d} \frac{x_i^2}{2} - \mu \sin^2(t) \sum_{i=1}^d x_i \Bigr)$ and coupled with homogeneous Dirichlet boundary conditions. The parameters are set to $\nu = \mu = 1$. After the space discretization with second order centered finite differences we obtain the stiff system of ODEs  
\[ 
\partial_t \psi_h(t)=  \Tilde{H}(t) \psi_h(t) \\
\]
where $\Tilde{H}$ refers to the potential after the spatial discretization. It can be integrated in time using the \emph{exponential midpoint rule} with time step $k=10^{-2}$

\begin{equation}
\psi_h^{n+1} = \exp\Bigl( k \Tilde{H}(t_n + k/2) \Bigr) \psi_h^{n}
\end{equation}

The exponential midpoint rule is an example of Magnus integrators, a class of exponential integrators which has been derived for highly oscillatory equations like \eqref{eqn:schroed}. We can show that solution surface $\{ (x,y,| \psi(x,y)| \}$ at time $T=1$.

\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{SchroedMumode.png}
\caption[Schrödinger equation]{Solution at time $T=1$ with $m=100$ points in each direction.}
\label{fig:Schroedmumo}
\end{figure}

and the relative heat map in Figure \eqref{fig:Schroedmumocolormap}.


\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{SchroedMumodecolormap.png}
\caption[Schrödinger equation - colormap]{Colormap of the surface above.}
\label{fig:Schroedmumocolormap}
\end{figure}



In this particular case, since the exponential depends on the current time step $t_n$, the exponentials of the small matrices have to be computed for every $n$. The computational time for the whole time integration with $m=20$ points in each direction produce \verb|Elapsed time is 0.081880 seconds|. Note that there's nothing special about the two dimensional case, indeed it can be extended easily to the $4$-dimensional case, and measuring the integration time yields \verb|Elapsed time is 0.196777 seconds|.


\begin{algorithm}
\caption{$\mu-$mode method for time integration up to time $T$}
\begin{algorithmic}
\State \% Spatial discretization
\State m = 20;
\State d = 4;
\State a = -10;
\State b = +10;
\State h = (b-a)/(m-1);
\State A = toeplitz(sparse([1,1],[1,2],[-2,1]/(h*h),1,m)); %Ax=Ay=Az = A
\State x = linspace(a,b,m);
\State y = linspace(a,b,m);
\State z = linspace(a,b,m);
\State l = linspace(a,b,m);
\State [X,Y,Z,L] = ndgrid(x,y,z,l);
\State $U_0 = \exp(-\sqrt{\nu}*(0.5)*(X*X + Y*Y + Z*Z + L*L));$ \%initial data (tensor)
\State \% Store time integration parameters k,T
\State t = 0.0
\State $U=U_0$
\While {$t+k<T$}
\State H = $\frac{A}{2} - \nu * \text{diag}(0.5*(x*x+y*y + z*z + l*l)) - \mu \sin(t+k/2) *\sin(t+k/2)*\text{diag}(x+y+z+l);$
\State H = 1j*k*H;
\State expmH = expm(H);
\State E\{1\} = expmH;
\State E\{2\} = expmH;
\State E\{3\} = expmH;
\State E\{4\} = expmH;
\State U = mmpnd(U,E); \%compute next time step
 \State t = t + k;
\EndWhile
\end{algorithmic}
\end{algorithm}
