

The bottleneck of rational methods is the solution of the arising linear systems.  An important property of the above systems is that the matrices are \emph{shifted} and \emph{complex symmetric}, i.e. $(A-\theta_j I)^T=A-\theta_j I$.  In \autocite{simoncini} some acceleration techniques to approximate the terms in the expansion \eqref{eqn:CRAMtrick} have been exploited. They choose an iterative method which is an appropriate refined version of the non-Hermitian Lanczos algorithm, that exploits the shifted complex symmetric form of the coefficient matrix, known as \emph{simplified QMR method} (see \cite{sQMR}). In order to speed up convergence, a preconditioner is applied, which can be the incomplete $LU$ factorization of the shifted matrix, or the $LDL^T$ factorization. However, the computational times reported in the paper are quite large. Therefore, some other methods/techniques should be investigated.


\section{Shifting and spectrum}

The complex shift adds an imaginary component to each eigenvalue, and this can reduce the condition number of the original matrix $A$. For instance, if $A$ is the five-point stencil matrix \verb|-gallery(`poisson',20)|, then its condition number $K_2(A) \approx 258$. Applying the shifts $\{ \theta_j \}_j$, $K_2(A-\theta_j I)$ decrease significantly, as reported in table \ref{table:conds}


\begin{center}
$
\begin{array}{lcc}

\toprule
\label{table:conds}

 \text{Shift} & \text{ $K_2(A-\theta_j I)$} \\
\midrule
\theta_1 &  1.5487 \\
\theta_2 &  1.7206 \\
\theta_3 & 1.8896 \\
\theta_4 & 2.2086 \\
\theta_5  & 2.3296 \\
\theta_6  & 2.3973 \\
\theta_7  & 2.0556 \\

\bottomrule
\end{array}
$
\captionof{table}{Condition number for each shifted systems}
\end{center}




\begin{figure}[h!]
\centering
\includegraphics[width=1.1 \textwidth]{spectrum_shifts.jpg}
\caption[Gershgorin's circles]{Gershgorin's circles and some of the eigenvalues for $A-\theta_j I$.}
\label{fig:spec_shift}
\end{figure}

Notice that since $A$ is symmetric, then Gershgorin's localization simplifies since $$ R_i = C_i =\{ z \in \mathbb{C} \colon |z-a_{ii}| \leq \sum_{j \ne i}^{n} |a_{ij}| \} $$

This implies, in this particular case $$\sigma \bigl( A- \theta_j I \bigr) \subset \{ z \in \mathbb{C} \colon |z-(-4 - \theta_j)| \leq 4 \}$$ In \ref{fig:spec_shift} the Gershgorin's circles in the complex plane are reported. The dot-dashed one corresponds to the spectrum of the original matrix $A$. The decrease of the condition number is desirable, since the convergence of iterative methods strongly depends on it.


\section{Arnoldi iteration}
Let $K_m(A,v)= \operatorname{span} \{v,Av,\ldots, A^{m-1} v \}$ the $m$-th dimensional Krylov subspace. Arnoldi's procedure is a method for building an orthogonal basis of $K_m(A,v)$ which relies on the Gram-Schmidt orthogonalization procedure. In \emph{exact} arithmetic, the algorithm is as follows:

\begin{algorithm}
\caption{Arnoldi }
\begin{algorithmic}
\State Choose $v$ and define $v_1=\frac{v}{\norm{v}}$ 

\For { $j=1,\ldots, m$}

\State Compute $h_{ij} = (A v_i,v_i)  \quad i=1,\ldots,j$
\State $w_j = A v_j - \sum_{i=1}^{j} h_{ij}v_i$
\State $h_{j+1,j} = \norm{w_j}_2$
\State  If $h_{j+1,j}=0$ then \emph{stop}
\State  $v_{j+1}=\frac{w_j}{\norm{w_j}_2}$
\EndFor

\end{algorithmic}
\end{algorithm}

If $h_{j+1,j} \ne 0$ for all $j =1,\ldots,m$, then the algorithm stops at step $m$, and the vectors $\{v_1,\ldots, v_m \}$ form an orthonormal basis for $K_m(A,v)$. Then, if we denote with $V_m$ the $n \times m$ matrix with $i-$th column the vector $v_i$,  and $H_m$ the $(m+1) \times m$ upper Hessenberg matrix with entries $h_{ij}$ we have the key relations: 

\begin{equation}
\label{eqn:arnoldi}
V_m^T A V_m = H_m \\
\end{equation}
\begin{equation}
A V_m = V_{m+1} H_m + h_{m+1,m} v_{m+1} e_{m+1}^T
\end{equation}

which are used to derive classical iterative methods like FOM, GMRES
\section{Lanczos procedure}

If the matrix $A$ is Hermitian, since $H_m = V_m^{\dagger} A V_m$ is Hermitian and also Hessenberg, it follows that $H_m$ must be a Hermitian tridiagonal matrix. This leads to a three-term recurrence for the Arnoldi iteration, which is therefore much cheaper. Indeed, if we define $H_m$ as

\[ 
H_m=
\begin{bmatrix}
\alpha_1 & \beta_2 & 0 &  0& \ldots &0 \\ 
\beta_2 & \alpha_2 & \beta_3 & 0 &\ldots & 0 \\ 
0 & \beta_3 & \alpha_3 &\beta_4 	&  \ldots & 0\\
\vdots & \ddots & \ddots & \ddots & \ddots  & \vdots \\
0& \ldots& \ldots& \beta_{m-1}&\alpha_{m-1}& \beta_m\\
0&\ldots &\ldots &0&\beta_m& \alpha_m
\end{bmatrix}
\]

the Lanczos procedure is as follows:


\begin{algorithm}
\caption{Lanczos }
\begin{algorithmic}
\State Choose $v$ and define $v_1=\frac{v}{\norm{v}}$ 
\State Set $\beta_1 = 0, v_0=0$

\For { $j=1,\ldots, m$}

\State $w_j=A v_j - \beta_j v_{j-1}$
\State $\alpha_j = (w_j,v_j)$
\State $w_j = w_j - \alpha_j v_j$
\State $\beta_{j+1} = \norm{w_j}_2$
\State  If $\beta_{j+1}=0$ then \emph{stop}
\State $v_{j+1}=\frac{w_j}{\beta_{j+1}}$
\EndFor

\end{algorithmic}
\end{algorithm}


In \emph{exact} arithmetic, the vectors $v_i$ are orthogonal, while in practice after some iterations there is a loss of orthogonality. To overcome this drawback, special techniques as \emph{look-ahead} are used in practice (see \cite{Lookahead}).

\section{QMR: Quasi-Minimal residual method}
In the following the main idea of the QMR method will be outlined. The original reference with all the details is \cite{QMR}. Let $K_m(A,r_0)= \operatorname{span} \{ r_0,A r_0,\ldots, A^{m-1} r_0 \} $ the Krylov subspace of dimension $m$. The QMR approach constructs iterates $x_n$ such that $x_n \in x_0 + K_m(A,r_0)$. As usual, define $r_n = b- A x_n$, $\rho_0 = \norm{r_0}$  and starting from $v_1 = \frac{r_0}{\rho_0}$ one can build the Lanczos vectors $\{ v_1, \ldots, v_m \}$. Then:

\begin{itemize}
\item $x_n = x_0 + V^{(m)} z$ for a suitable $z \in \mathbb{C}^m$ to be determined.
\item let $H_e^{(n)} =  \begin{bmatrix} 
H^{(n)} \\
\rho_{m+1} e_n^T
\end{bmatrix}$, so we have $A V^{(m)} = V^{(m+1)} H_e^{(m)}$. 

\item Then, the residual satisfies $$r_n = b-A x_n = r_0 - V^{(m+1)} H_e^{(m)} z = V^{(m+1)} \bigl( \rho_0 e_1^{(m+1)} - H_e^{(m)} z \bigr)$$

Ideally, one would choose $z \in \mathbb{C}^m$ s.t. $\norm{r_n}$ is minimal, as in GMRES. Note, however, that $V^{(n+1)}$ is not unitary, therefore the $2$-norm is changed. Instead, it has been chosen to minimize only the norm of the bracketed term.

\item $z = \operatorname{argmin}_{z \in \mathbb{C}^m} \norm{\rho_0 e_1^{(m+1)} - H_e^{(m)} z}$ which is a cheap to solve least squares problem. 

\item A $QR$ decomposition of $H_e^{(n)}$ is used to solve the least squares problem.


\end{itemize}



\begin{algorithm}
\caption{General QMR}
\begin{algorithmic}
\State Choose $x_0 \in \mathbb{C}^n$ and set $r_0=b- A x_0$, $\rho_0=\norm{r_0}$, $v_1 = \frac{r_0}{\rho_0}$
\For { $n=1,\ldots, \text{until convergence}$ }
\State Build matrices $V^{(m)}, V^{(n+1)}, H_e^{(n)}$ with Lanczos procedure
\State Update QR factorization $H_e^{(n)}$ and obtain $z^{(n)}$ from the least squares problem
\State $x_n \gets x_0 + V^{(n)} z^{(n)}$
\EndFor
\end{algorithmic}
\end{algorithm}


Implementation details are also described in \cite{QMR}. In particular, by exploiting the special structure of the $QR$ factorization, $x_n$ can be computed with short recurrences.

\subsection{QMR and Preconditioning}

The use of a preconditioner $M$ in a QMR method is as follows: assume $M$ can be decomposed in $M=M_1 M_2$. The strategy is to apply QMR to the system $$A' y = b'$$

where $A' = M_1^{-1} A M_2^{-1},\quad  b' = M_1^{-1}(b-A x_0), \quad y=M_2 (x-x_0)$. Starting from this relations, and applying the QMR method to the preconditioned system, one obtains iterates $y_n$ and residuals $\tilde{r_n}$ which can be transformed back into the corresponding quantities $x_n$ and $r_n$ by setting:
\begin{align}
x_n = x_0 + M_2^{-1}y_n \\
r_n = M_1  \tilde{r_n}
\end{align}

As a preconditioner, an incomplete LU decomposition is applied with \verb|droptol| $=10^{-2}$, which can be performed in MatLab with the command \verb|ilu|. To see the goodness of preconditioning for our purposes, consider the following

\subsection{Test}

Let $A=-$\verb|gallery(`Poisson',200)| and $b$ some normally distributed random vector.  We can compute $\eqref{eqn:CRAM}$ by using the MatLab \verb|qmr| method for the seven shifted linear systems with initial guess the previously computed solution. As a preconditioner an incomplete $LU$ decomposition is used. In the second figure, we can see the effect of preconditioning.


\begin{figure}[H]
\centering
\subfloat[][\emph{No preconditioning}.]
{\includegraphics[width=0.8\textwidth]{NO_preconditioningQMR_poisson.jpg}} \quad
\subfloat[][\emph{With preconditioning}.]
{\includegraphics[width=0.8\textwidth]{ILU_preconditioningQMR_poisson.jpg}}
\caption[QMR and preconditioning example]{Convergence plots. Note the different values on the x-axis.}
\end{figure}


In both cases, the system corresponding to the shift $\theta_6$ exhibits the slowest convergence speed. This behavior is due to the condition number of $A-\theta_6 I$. Notice also that for $n=200$, $K_2(A) = 2.38 \cdot 10^{4}$, but the shifted systems are much more well conditioned.





\section{Krylov subspace methods for shifted complex symmetric systems}
A different strategy is to exploit the shifted structure of the systems. All this type of methods use the following 

\begin{theorem}[Shift invariance of Krylov subspaces]
\label{thm:shift_thm}
Let $A$ real symmetric matrix and $K_n(A,r_0)=\operatorname{span} \{ r_0, A r_0, \ldots, A^{n-1}r_0 \}$ the n-dimensional Krylov subspace. For $\sigma_1, \sigma_2 \in \mathbb{C}$, we have

\[
K_n(A +\sigma_1 I, r_0) = K_n(A+\sigma_2 I, r_0)
\]

\end{theorem}

\begin{proof}

Note that $\bigl( A+\sigma_1 I \bigr)^k r_0= \sum_{j=0}^{k} \binom{k}{j} \sigma_1^{k-j} A^j r_0 \in \operatorname{span} \{ A^k r_0, k=0,\ldots,n-1 \} = K_n(A,r_0)$. 

Therefore $$K_n(A+\sigma_1 I,r_0) \subset K_n(A,r_0) $$

Now, $$K_n(A+\sigma_1, r_0) \subset K\Bigl( \bigl[ A + \sigma_1 I\bigr] + \bigl[ \sigma_2-\sigma_1 \bigr] I \Bigr)=K_n(A+\sigma_2I,r_0)$$ which proves the first inclusion. The other follows just by exchanging $\sigma_1$ with $\sigma_2$.

\end{proof}

This means that only one Krylov subspace  for solving the shifted linear systems must be generated.  

\subsection{COCR method for complex symmetric shifted matrices}

The Conjugate A-Orthogonal Conjugate Residual (COCR), developed in \cite{COCR}, is a Krylov subspace method for complex symmetric linear systems. Let $x_n$ be the $n-$ th iteration of the method, $r_n = b-A x_n$ the corresponding residual and $p_n$ the search direction. We can can sketch its derivation, starting with
\begin{itemize}
\item $p_0 = r_0$
\item $r_n = r_{n-1} - \alpha_{n-1} Ap_{n-1}$
\item $p_n = r_n +\beta_{n-1} p_{n-1}$
\end{itemize}

Now, in order to find formulas for $\alpha_{n-1}$ and $\beta_{n-1}$, we impose the following orthogonality conditions:

\[
r_n  \perp W, \quad A p_n \perp W
\]

where $W=\overline{A} K_n(\overline{A},\overline{r_0})$. To determine $\alpha_{n-1}$ one can plug the formula for $r_n$ in $$(\overline{A^n}, \overline{r_0},r_n) = (\overline{A^n}, \overline{r_0},r_{n-1}) - \alpha_{n-1} (\overline{A^n} r_0, A p_{n-1}) $$ By the first condition, we obtain 
\begin{itemize}
\item $\alpha_{n-1}= \frac{(\overline{A^n}  \overline{r_0}, r_{n-1})}{(\overline{A^n}  \overline{r_0},A p_{n-1})}$
\end{itemize}

Analogously, $\beta_{n-1}$ can be determined from $$(\overline{A^n} \overline{r_0}, A p_n) = (\overline{A^n} \overline{r_0}, A r_n) + \beta_{n-1} (\overline{A^n} \overline{r_0},A p_{n-1})$$
by imposing the second condition, which yields

\begin{itemize}
\item $\beta_{n-1} = -\alpha_{n-1}  \frac{(\overline{A^n}  \overline{r_0}, A r_n)}{(\overline{A^n}  \overline{r_0}, r_{n-1})}$
\end{itemize}

Such formulas for $\alpha_{n-1}, \beta_{n-1}$ are still of no practical usage, since they involve matrix powers and much matrix-vector products. It can be proved using the recurrences for $r_n$ and $\alpha_n$ (see \cite{COCR} for the details) that 

\begin{itemize}
\item $\alpha_{n-1} = \frac{(\overline{A} \overline{r_{n-1}},r_{n-1})}{(\overline{A} \overline{p_{n-1}}, A p_{n-1})}$
\item $\beta_{n-1} = \frac{(\overline{A} \overline{r_n},r_n)}{(\overline{A} \overline{r_{n-1}}, r_{n-1})}$
\end{itemize}




\begin{algorithm}
\caption{COCR method for complex symmetric systems}
\begin{algorithmic}
\State Choose $x_0 \in \mathbb{C}^n$ and set $r_0=b- A x_0$, $\rho_0=\norm{r_0}$, $v_1 = \frac{r_0}{\rho_0}$
\State $p_0 = 0$ and $\beta_{0} = 0$
\For { $n=1,\ldots, \text{until } \norm{r_n} \leq \varepsilon \norm{b}$ }
\State $p_n \gets r_n + \beta_{n-1}p_{n-1}$

\State Save product $A p_n$
\State $\alpha_n \gets \frac{(\overline{r_n}, A r_n)}{(\overline{A} \overline{p_n}, A p_n)}$
\State
\State $\beta_n \gets \frac{(\overline{r_n}, A r_n)}{(\overline{A} \overline{p_n}, A p_n)}$
\State $x_{n+1} \gets x_n + \alpha_n p_n$
\State $r_{n+1} \gets r_n - \alpha_n A p_n$
\State $\beta_{n+1} \gets \frac{(\overline{r_n+1}, A r_{n+1})}{(\overline{r_n} , A r_n)}$
\EndFor
\end{algorithmic}
\end{algorithm}

Also, if no breakdown occurs, then the $n-$th residual vector satisfies $$r_n \perp \overline{A} K_n(\bar{A},\bar{r_0})$$


which leads to \emph{A-conjugate orthogonality} $(\overline{r_i}, A r_j) = 0$ for $i \ne j$. The residual vector $r_n$ can be written as $R_n(A) r_0$, where $R_n (\cdot)$ is a polynomial that satisfies



\begin{align}
\label{eqn:poly}
R_0(\lambda) = 1 \\
R_1(\lambda) = (1- \alpha_0 \lambda) R_0(\lambda) \\
R_n(\lambda)= \bigl( 1+ \frac{\beta_{n-2}}{\alpha_{n-2}} \alpha_{n-1} - \alpha_{n-1} \lambda \bigr) R_{n-1} \lambda - \frac{\beta_{n-2}}{\alpha_{n-2}} \alpha_{n-1} R_{n-2}(\lambda), \quad n = 2,\ldots
\end{align}

In the following table the number of inner products (IP), $ax + y$ (AXPY), and matrix vector products per iteration (MV) are reported.


\begin{center}
$
\begin{array}{lccc}

\toprule
\label{tab:costCOCR}

 \text{Method} & \text{IP} & \text{AXPY} & \text{MV} \\
\midrule
\text{COCR} &  2 &4 &1 \\
\bottomrule
\end{array}
$
\captionof{table}{COCR operations}
\end{center}



\subsection{Extension to several complex symmetric linear systems}

In this section the problem
\begin{equation}
\label{eqn:systems}
(A + \sigma_k I) x^{(k)} = b \text{ for } k \in \{1, \ldots, M \}
\end{equation}

is considered, where $A$ is a symmetric sparse matrix, and the shift is complex. This is interesting because it is the setting of \eqref{eqn:CRAM}. Following the authors in \cite{shiftCOCR}, the derivation can be done just by using two linear systems: $A x = b$, called \emph{seed system}, and $(A+\sigma_1 I)x^{(1)} = b$. From the shift invariance theorem \ref{thm:shift_thm}, we can use the information of residuals $r_0, \ldots, r_n$ to solve $(A+\sigma_1 I)x^{(1)} = b$. To this aim, the authors take the following \emph{collinear residual approach}:  

\begin{equation}
\label{eqn:colres}
 r_n = \pi_n^{(1)} r_n^{(1)}
 \end{equation}

with $\pi_n^{(1)} \in \mathbb{C}$.

This allows to have a formula for updating $r_{n+1}^{(1)}$ in terms of $r_{n+1}$. Indeed (using the polynomial recurrence \ref{eqn:poly}):

\begin{equation}
\label{eqn:rnew}
r_{n+1}^{(1)}  = \Bigl ( 1 + \frac{\beta_{n-1}}{\alpha_{n-1}} \alpha_n - \alpha_n A     \Bigr ) r_n -  \frac{\beta_{n-1}}{\alpha_{n-1}} \alpha_n r_{n-1}
\end{equation}

The residual for the shifted system is computed analogously 

\begin{equation}
\label{eqn:rnewl}
r_{n+1}^{(1)} = \Bigl \{  1 + \frac{\beta_{n-1}^{(1)}}{\alpha_{n-1}^{(1)}} \alpha_n^{(1)} - \alpha_n^{(1)} (A + \sigma_1 I)     \Bigr \} r_n^{(1)} - \frac{\beta_{n-1}^{(1)}}{\alpha_{n-1}^{(1)}} \alpha_n^{(1)} r_{n-1}^{(1)} 
\end{equation}

Using the collinearity of residuals \ref{eqn:colres}

\begin{equation}
\label{eqn:rnew1}
r_{n+1}^{(1)} = \Bigl \{  1 + \frac{\beta_{n-1}^{(1)}}{\alpha_{n-1}^{(1)}} \alpha_n^{(1)} - \alpha_n^{(1)} (A + \sigma_1 I)     \Bigr \} \frac{\pi_{n+1}^{(1)}}{\pi_n^{(1)}} r_n - \frac{\beta_{n-1}^{(1)} \pi_{n+1}^{(1)}}{\alpha_{n-1}^{(1)} \pi_{n-1}^{(1)}} \alpha_n^{(1)} r_{n-1}^{(1)}
\end{equation}


Therefore, the parameters $\alpha_n^{(1)}, \beta_{n-1}^{(1)}$ and $\pi_{n+1}^{(1)}$ are required. To compute them, we can start comparing: 

\begin{itemize}
\item the coefficients of $A r_n$ in \eqref{eqn:rnew} and \eqref{eqn:rnew1}. This yields $\alpha_n^{(1)} = \frac{\pi_n^{(1)}}{\pi_{n+1}^{(1)}} \alpha_n$

\item the coefficients of $r_{n-1}$, which leads to $\beta_{n-1}^{(1)} =  \Bigl( \frac{\pi_{n-1}^{(1)}}{\pi_{n}^{(1)}} \Bigr) ^2 \beta_{n-1} $

\item the coefficients of $r_n$, which leads to $\pi_{n+1}^{(1)} = \Bigl (  1 + \frac{\beta_{n-1}}{\alpha_{n-1}} \alpha_n + \alpha_n \sigma_1  \Bigr )  \pi_n^{(1)}  - \frac{\beta_{n-1}}{\alpha_{n-1}} \alpha_n \pi_{n-1}^{(1)} = R_{n+1}(-\sigma_1)$
\end{itemize}


Now that we have all the coefficients to update $r_{n+1}^{(1)}$, we can give the formulas for updating $x_{n+1}$. From \ref{eqn:rnewl}, defining $p_{n-1}^{(1)} = (A+\sigma_1 I)^{-1} (r_{n-1}^{(1)} - r_n^{(1)}) / \alpha_{n-1}^{(1)}$, it follows 

\begin{itemize}
\item $p_n^{(1)}=r_n^{(1)} + \beta_{n-1}^{(1)} p_{n-1}^{(1)} = \frac{r_n}{\pi_n^{(1)}} + \beta_{n-1}^{(1)} p_{n-1}^{(1)}$ \\
\item  $r_{n+1}^{(1)} = r_n^{(1)} - \alpha_n^{(1)} (A + \sigma_1I) p_n^{(1)} $
\item $x_{n+1}^{(1)} = x_n^{(1)} + \alpha_n^{(1)} p_n^{(1)} $
\end{itemize}





The above derivation can be readily generalized to solve \eqref{eqn:systems}, choosing as seed system one among the $M$ available. In the pseudocode the superscript $s$ corresponds to the parameters for the seed system. In the original paper \cite{shiftCOCR} there are some errors in the code, as some superscripts are missing.
\begin{algorithm}
\caption{Shifted COCR}
\begin{algorithmic}
\State Choose a seed system $s \in \{ 1,\ldots,M  \}$
\State Set $r_0=b$, $\beta_{-1}^{(s)}=0$
\State Set $x_0^{(k)} = p_{-1}^{(k)}=0$ for $ k \ne s$ 
\State $\pi_0^{(k)} = \pi_{-1}^{(k)} = 1$ for $k \ne s$

\For { $n=1,\ldots, \text{until } \norm{r_n^{(s)}} \leq \varepsilon_1 \norm{b}$ }
\State $p_n^{(s)} \gets r_n^{(s)} + \beta_{n-1}^{(s)} p_{n-1}^{(s)}$
\State Save $A (\sigma_s) p_n^{(s)} = A(\sigma_s) r_n^{(s)} + \beta_{n-1}^{(s)} A(\sigma_s) p_{n-1}^{(s)}$
\State $\alpha_n^{(s)} \gets \frac{(\overline{r_n^{(s)}}, A(\sigma_s) r_n^{(s)})}{(\overline{A(\sigma_s)} \overline{p_n^{(s)}}, A(\sigma_s) p_n^{(s)})}$
\State $x_{n+1}^{(s)} \gets x_n^{(s)} + \alpha_n^{(s)} p_n^{(s)}$


\For { $k  ( \ne s ) =1,2, \ldots, M $ }
\If { $\norm{r_n^{(k)}} > \varepsilon_2 \norm{b}$ }
\State $\pi_{n+1}^{(k)} \gets R_{n+1}(\sigma_s - \sigma_k)$
\State $\beta_{n-1}^{(k)} \gets \Bigl( \frac{\pi_{n-1}^{(k)}}{\pi_n^{(k)}} \Bigr) \beta_{n-1}^{(s)}$
\State $\alpha_n^{(k)} \gets \frac{\pi_n^{(k)}}{\pi_{n+1}^{(k)}} \alpha_n^{(s)}$
\State $p_n^{(k)} \gets \frac{1}{\pi_n^{(k)} } r_n^{(s)} + \beta_{n-1}^{(k)} p_{n-1}^{(k)}$
\State $x_{n+1}^{(k)} \gets x_n^{(k)} + \alpha_n^{(k)} p_n^{(k)}$


\EndIf
\EndFor


\State $r_{n+1}^{(s)} \gets r_n^{(s)} - \alpha_n^{(s)} A(\sigma_s) p_n^{(s)}$
\State $\beta_{n}^{(s)} \gets \frac{ (\overline{r_{n+1}^{(s)}}, A(\sigma_s) r_{n+1}^{(s)} )}{(\overline{r_n^{(s)}} , A(\sigma_s) r_n^{(s)} )}$
\EndFor
\end{algorithmic}
\end{algorithm}


\subsection{Implementation and remarks}

It's important to note that we obtain successive approximations $x_{n+1}^{(1)}$ with one matrix-vector multiplication per step. There is no need to compute the inner product $\norm{r_n^{(1)}}$, since it's already available by $r_n$ via $$ \norm{ r_n^{(1)} } = \frac{\norm{r_n}}{\vert  \pi_n^{(1)} \vert}$$

Also, only two new inner products need to be computed at each iteration. If we use the COCR method $M$ times, the overall cost has to be multiplied by $M$, so it is clearly higher, as reported in the next table

\begin{center}
$
\begin{array}{lccc}

\toprule


 \text{Method} & \text{IP} & \text{AXPY} & \text{MV} \\
\midrule
\text{COCR} &  2M & 4M &M \\
\text{Shifted COCR} &  2 & 2(M+1) & 1 \\
\bottomrule
\end{array}
$
\captionof{table}{Summary of operations}
\end{center}

Finally, for each of the $M-1$ systems, we can use a three-term recurrence for the $\pi$-parameters, since at iteration $i$ we need values $\pi_{i-1}^{(k)}, \pi_i^{(k)}, \pi_{i+1}^{(k)}$ for $k \ne s$.


%\section{Numerical experiments}
