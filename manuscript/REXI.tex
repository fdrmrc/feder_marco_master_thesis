

This rational approximation method consists in a technique to approximate $e^A v$ when $A$ has purely imaginary eigenvalues. This fact is used in its derivation, which consists of the following steps: 

\begin{itemize}
\item approximation of the scalar function $e^{ix},  x \in \mathbb{R}$ as sum of  Gaussian functions
\item approximation of the Gaussian functions as sum of rational functions
\item extension to the matrix case and solution of the arising linear systems
\end{itemize}

This brings several benefits from the computational point of view, since it makes the process highly parallelizable. The main reference is \cite{REXIorig}, but an efficient correction of the original paper has been made by  A. Moriggl in his master's thesis \cite{tesiAM}. In the following, just the main points will be repeated.

\subsection{Step 1: from $e^{ix}$ to Gaussians}
Let $m  \in \mathbb{Z}, h \in \mathbb{R}$ and $\psi_h(x)= \frac{1}{\sqrt{4 \pi}} e^{-\frac{x^2}{h^2}}$ the Gaussian kernel and $b_m=e^{h^2} e^{-imh}$. By writing $e^{ix}$ as linear combination of Gaussian functions and using the Fourier transform properties one gets
$$e^{ix} \approx  \sum_{m = -M}^{M} b_m \psi_h(x+mh)$$

where $b_m=h \int_{-\frac{1}{2h}}^{\frac{1}{2h}} e^{-2 \pi i m h w} \frac{\hat{f}(w)}{\hat{\psi_h}(w)}  \text{d}w $ and $f(w)=e^{i w}$. Therefore, since $$\hat{f}(w)=\delta(w-\frac{1}{2 \pi})$$ and $$\hat{\psi_h}(w) = h e^{-4 \pi^2 h^2 w^2}$$ we obtain 

\begin{equation}
b_m =  \int_{-\frac{1}{2h}}^{\frac{1}{2h}}  e^{- 2 \pi i m h w} \frac{\delta(w - \frac{1}{2\pi} )}{h e^{-4 \pi^2 h^2 w^2}}  \text{d} w = e^{- \frac{2 \pi i m h}{2 \pi}} \Bigl(  e^{-\frac{4 \pi^2 h^2}{(2 \pi)^2}} \Bigr)^{-1}v
\end{equation}

Note that if $h>\pi$, then $\delta(w - \frac{1}{2 \pi}) =0$ because $[-\frac{1}{2h}, \frac{1}{2h}]$ does not contain $\frac{1}{2\pi}$. Therefore it has to be $h \leq \pi$, even if in practice a suitable value will be much smaller. The two parameters $M$ and $h$ are crucial for the accuracy of the approximation and their choice will be discussed later on.

\subsection{Step 2: from Gaussians to rational functions}

Consider the symmetric rational function $R(x)= \text{Re} \bigl( \sum_{l=-L}^{L} \frac{a_l}{ix + \mu + il} \bigr)$. The goal is to find coefficients $\{a_l\}_l$ that minimize the $l^2$ error 

\[ 
\norm*{ \Bigl(  \frac{1}{\sqrt{4 \pi}} e^{-x_k^2/4} -  R(x_k) \Bigr)_{k=1}^K}_{2}
\]

where $\{ x_k \}_k$ is a set of points in the symmetric interval $[-R,R]$ for $R$ large enough. To solve this minimization problem it's useful to write the evaluation of the symmetric function as a matrix-vector product: $R(x_k)=A(x_k,\mu,L) y \approx \psi_1(x_k)$, with $y$ to be determined. Here $A \in \mathbb{R}^{1 \times (2L+1)}$ and $y \in \mathbb{R}^{2L+1}$ 
\[
 y =[a_0, \text{Re}(a_1), \ldots, \text{Re}(a_L), \text{Im}(a_1), \ldots, \text{Im}(a_L)]^T 
\]

Therefore, considering the set of points $X_K = [x_1, \ldots,x_K]$, we can introduce  

\[
A(X_K,\mu,L)=
\begin{bmatrix}
A(x_1,\mu,L) \\
\vdots \\
A(x_K,\mu,L)
\end{bmatrix} 
\in \mathbb{R}^{K \times (2L+1)}
\]

and hence the linear system $$A(X_K,\mu,L)y = \psi_1(X_K) = [\psi_1(x_1), \ldots, \psi_1(x_K)]^T$$ The set of points is determined iteratively with the Leja approach (see \cite{Reichel}), i.e. starting with $x_1=0$ and defining $$x_{k+1} = \text{arg max}_{[-R,R]} |R_k(x) - \psi_1(x) |$$ where $R_k(x)$ is the approximation of $R(x)$ with coefficients given by  the points $[x_1,\ldots , x_k]$ previously computed, i.e. $y_K$ is the least square solution of $A(X_K,\mu,L)y_K =  \psi_1(X_K)$. In this way, linear systems (in the least square sense) are iteratively solved, adding at each step a row that depends on the latest point $x_k$. The final approximation of $R(x)$ is built choosing the index $\bar{k}$ for which the $l^{\infty}$ error \[ \max_{x \in [-R,R]} |R_k(x)-\psi_1(x)|\] is the smallest, where $[-R,R]$ is the set of points used to estimate this error. It has been chosen  $R=250$, according to the authors in \cite{REXIorig}. In the following, one can appreciate the plot of the absolute error $|\psi_1(x) - R(x)|$ for the chosen coefficients. 

\begin{figure}[h]
\center
\includegraphics[width=0.8 \textwidth]{coeffs_error.jpg}
\caption[Rational approximation error (REXI)]{Approximation error. The parameters used to obtain the plot above are $K=100, L=24, \bar{k}=49$ and $\mu \approx -5.13$.}
\label{fig:errpltREXI}
\end{figure}

As can be seen in Figure \ref{fig:errpltREXI}, the $l^{\infty}$ approximation error is below $7 \times 10^{-15}$. 


\subsection{Step 3}
Combining the two steps above we have 

\begin{equation}
\label{eqn:REXIa}
e^{ix} \approx \sum_{m=-M}^M b_m \sum_{l=-L}^L \text{Re} \Bigl( \frac{h a_l}{ix + h(\mu + i(m+l))} \Bigr)
\end{equation}

Defining \[ \beta_n^{Re} = h \sum_{m=-M}^M \sum_{l=-L}^L \text{Re}(b_m) a_l \delta_{n,m+l} \]
and
\[ \beta_n^{Im} = h \sum_{m=-M}^M \sum_{l=-L}^L \text{Im}(b_m) a_l \delta_{n,m+l} \]
and $N = M + L$, we can finally write 



\begin{equation}
\label{eqn:REXI}
e^{ix} \approx  \sum_{n=-N}^N \text{Re} \Bigl( \frac{\beta_n^{Re}}{ix+\alpha_n} \Bigr) + i\text{Re} \Bigl( \frac{\beta_n^{Im}}{ix+\alpha_n} \Bigr)
\end{equation}
where $\alpha_n=h(\mu + i n)$.


\subsubsection{Alternative step 3}

The sum \eqref{eqn:REXIa} can be computed also in another way, by defining 
\[
n = m +l, \quad
N = M+L, \quad
\alpha_n = h ( \mu + i n)
\]


and \[ c_{1,n} = h \sum_{m = -M}^M \sum_{l=-L}^L \text{Re}(a_l) b_m \delta_{n,m+l} \]
\[ c_{2,n} = h \sum_{m=-M}^M \sum_{l=-L}^L \text{Im}(a_l) b_m \delta_{n,m+l} \]

so that the approximation is 
\begin{equation}
\label{eqn:altREXI}
e^{ix} \approx \sum_{n=-N}^N \frac{c_{1,n} h \mu + c_{2,n} (x+hn)}{(\alpha_{-n} - ix)(\alpha_n + ix)}
\end{equation}



\section{REXI parameters $M$ and $h$}

From the scalar error analysis performed in \cite{tesiAM} it follows that we can determine the parameter $M$, after fixing $h$ a priori, from the inequality

\begin{equation}
\label{eqn: ineqREXI}
|x| \leq (M-m_0)h
\end{equation}

where $m_0=11$ has been determined experimentally in \cite{tesiAM}. To see the goodness of the approximation in the scalar case, we plot in a log scaled figure

\[ E_{\text{rel}}(M,h) = E_{\text{abs}}(M,h) = \frac{|e^{30i} - \text{REXI}(30i,M,h)|}{|e^{30i}|} = |e^{30i} - \text{REXI}(30i,M,h)| 
\]

for different values of $h$ as a function of $M$. We can see that, depending on $h$, after a certain $M$ the error curve drops for the values of $M$  such that $|x| \leq (M-m_0)h$. Also, it's clear from Figure \eqref{fig:scalaREXI} that the accuracy doesn't improve for larger values of $M$ and that the best results are obtained for $h \in [0.3,0.6]$. Moreover, if $h$ is too big or too small, this leads a larger value for $M$, which implies more summation terms.

\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{scalaREXIgrid.png}
\caption[Error curve REXI]{Error curve for different values of $h$ as a function of $M$.}
\label{fig:scalaREXI}
\end{figure}


\section{Extension to matrix case}

Hereafter we assume $\sigma(A) \subset i \mathbb{R}$. Formally, one could substitute $\tau A$ in place of $i x$ in \eqref{eqn:REXI}, but this has to be justified by a diagonalization argument. So, assume $A$ to be a real and diagonalizable matrix. Hence, $A = VEV^{-1}$  and by definition of matrix exponential we have \[ e^{\tau A} = V e^{\tau E} V^{-1}\] where $\Bigl( e^{\tau E} \Bigr)_{ii} = e^{\tau i \lambda_i}$, since $A$ has purely imaginary eigenvalues. Then we have 

\[ 
\sum_{n=-N}^N \text{Re} \Bigl( \beta_n^{Re} (\tau A+\alpha_nI )\Bigr)^{-1} + i\text{Re} \Bigl( \beta_n^{Im}(\tau A+\alpha_n I ) \Bigr)^{-1} =
\]
\[
=  \sum_{n=-N}^N \text{Re} \Bigl( \beta_n^{Re} V(\tau E+\alpha_nI )^{-1}  V^{-1}\Bigr)^{-1} + i\text{Re} \Bigl( \beta_n^{Im}V(\tau E+\alpha_n I ) ^{-1} V^{-1} \Bigr)
\]

In the original reference \cite{REXIorig}, the authors pulled out the eigenvector matrix $V$ from the real part of the summation above. But, as noted in the correction \cite{tesiAM}, this is false in general since, for instance, for skew symmetric real matrices it holds that $V \in \mathbb{C}^{n \times n}$, and therefore it's not true that $\text{Re}(V C V^{-1}) = V \text{Re}(C) V^{-1}$, where $C$ stands for the sum of inverses one would obtain after have pulled $V$ out.  Therefore, different situations has to be considered, depending both on the matrix $A$, the eigenvector matrix $V$ and the vector $v$:
\begin{itemize}
\item $A \in i \mathbb{R}^{d \times d}$, $V \in \mathbb{R}$ and $v \in \mathbb{R}^d$. 
\item $V \in \mathbb{C}^{d \times d}$ and $v \in \mathbb{C}^d$.
\end{itemize}


\subsection{First case}

Matrices of this kind are purely imaginary skew Hermitian matrices, like the finite difference discretization of the \emph{free Schrödinger} operator $i \partial_{xx}$ with periodic boundary conditions. Since also the vector $v$ is real, using the argument above we can write

\begin{equation}
\label{eqn:fcase}
e^{\tau A} v \approx \sum_{n=-N}^N \text{Re} \Bigl( \beta_n^{Re} (\tau A+\alpha_nI )^{-1} v\Bigr) + i\text{Re} \Bigl( \beta_n^{Im}(\tau A+\alpha_n I )^{-1} v \Bigr)
\end{equation}

Therefore, at each iteration we need to solve a linear system with matrix $\tau A + \alpha_n I$. If the vector $v$ was complex, i.e. $v= \text{Re}(v) + i \text{Im}(v)$, then we have $e^{\tau A} v =e^{\tau A} \text{Re}(v) + i  e^{\tau A} \text{Im}(v)$, while if we apply directly \eqref{eqn:fcase} the complex unit $i$ modifies all the coefficients $\beta_n$, and hence the method does not work anymore. So, if a complex vector is used, the method has to be applied twice.

\subsection{Second case}

This case is the more general, because in principle a matrix has complex eigenvectors. To avoid problem with the real part of a product of complex matrices, it's convenient to use the alternative form \eqref{eqn:altREXI}, which yields 

\begin{equation}
\label{eqn:TBreduced}
e^{\tau A} v \approx \sum_{n=-N}^N  \Bigl(  c_{1,n} h \mu I + c_{2,n}(- i \tau A+ h n I) \Bigr)  \Bigl(  \alpha_{-n}I - \tau A \Bigr)^{-1} \Bigl(  \alpha_n I + \tau A \Bigr)^{-1} v
\end{equation}

The computational cost here is higher, since for each summand, two different linear systems has to be solved. In the special case that both the matrix $A$ and the vector $v$ are real, we can reduce the work by a factor of two observing the following identities:

\[
c_{1,-n} = \overline{c_{1,n}} \quad
c_{2,n} = -\overline{c_{2,-n}} \quad
\alpha_n = \overline{\alpha_{-n}}
\]

and hence \eqref{eqn:TBreduced} becomes 

\[
e^{\tau A} v \approx \text{Re} \Bigl( \sum_{n=0}^N \Gamma_n (  c_{1,n} h \mu I + c_{2,n}(- i \tau A+ h n I) )  (  \alpha_{-n}I - \tau A )^{-1} (  \alpha_n I + \tau A )^{-1} v \Bigr)
\]

where $\Gamma_0 = 1$ and $\Gamma_n = 2$ for $n \geq 1$. 

In both cases we have a clearer error estimate which can be derived by the diagonalization argument applied before: 

\begin{equation}
\norm{\text{REXI}(\tau A, M ,h) - e^{\tau A}}  \leq \norm{V \text{REXI}(\tau E,M,h) V^{-1}} \leq \text{cond}(V) \max_{ \lambda_j \in \sigma(A)} |\text{REXI}(\tau \lambda_j, M,h) - e^{\tau \lambda_j}|
\end{equation}

and from the inequality \eqref{eqn: ineqREXI}  we need 
\begin{equation}
\label{eqn:ineqREXImatr}
 \tau \lambda \leq (M-m_0) h
 \end{equation}
to hold in order to have an accurate approximation.

