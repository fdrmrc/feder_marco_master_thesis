

Now, we consider instead the approximation of $e^{x}$ on $x \in (-\infty,0]$. We seek for a rational function $R(x)$ such that 

\[
\norm{e^x - R(x)} = \inf_{r(x)} \norm{e^x - r(x)}
\]

where the $\norm{\cdot}$ is the supremum norm, and the $\inf$ is over all the rational functions with degree $(m,n)$. Standard results in approximation theory ensure that such a function exists and is unique \cite{expxm}. The most common choiche in literature is to use the following, where $\nu = 14$ \[ R_{\nu}(x) =\alpha_0 +  \sum_{j=1}^{\nu} \frac{\alpha_j}{x - \theta_j} \]

which is called \emph{Chebychev partial fraction decomposition} and satisfies the following absolute error bound 
\begin{equation}
\label{eqn:chebound}
%\sup _{x \leq 0} |e^x - R_{\nu}(x) | \approx 10^{-\nu} 
\norm{e^x - R_{\nu}(x) } \approx 10^{-\nu} 
\end{equation}

Since $e^x - R(x)$ is unbounded on $[0,\infty)$ for every rational function $R(x)$, such a uniform approximation can't exist for positive $x$. The coefficients $( \alpha_j)_j$ and $(\theta_j)_j$ are tabulated in the literature (see \cite{chebCoeffs1}). It's worth mentioning that  this original coefficients' table contains errors and result in $10^2$ times poorer accuracy than expected by theory. Not only, popular software packages like EXPOKIT (\cite{expokit}) have \verb|FORTRAN| routines that use those inaccurate coefficients. As a matter of fact, if the action is computed with that routines, the absolute error with respect to a reference solution will have order $10^{-12}$, instead of $10^{-14}$. 
To illustrate this, Figure \ref{fig:errcoeffs} shows the absolute error curve $|e(x)-R_{14}(x)|$ computed using these coefficients.


\begin{figure}[h!]
\center
\includegraphics[width=0.9 \textwidth]{coeffs_saad_error.jpg}
\caption[Error in approximation of $e^x$ for $x \leq 0$]{Error in approximation of $e^x$ for $x \leq 0$ using Saad and Gallopoulos coefficients}
\label{fig:errcoeffs}
\end{figure}



For the ease of reproducibility, I report here the original coefficients computed in the original reference \cite{chebCoeffs1}

\begin{verbatim}
alpha0   =  0.183216998528140087E-11;
alpha(1) =  0.557503973136501826E+02 - 1j*0.204295038779771857E+03;
alpha(2) = -0.938666838877006739E+02 + 1j*0.912874896775456363E+02;
alpha(3) =  0.469965415550370835E+02 - 1j*0.116167609985818103E+02;
alpha(4) = -0.961424200626061065E+01 - 1j*0.264195613880262669E+01;
alpha(5) =  0.752722063978321642E+00 + 1j*0.670367365566377770E+00;
alpha(6) = -0.188781253158648576E-01 - 1j*0.343696176445802414E-01;
alpha(7) =  0.143086431411801849E-03 + 1j*0.287221133228814096E-03;

theta(1) = -0.562314417475317895E+01 + 1j*0.119406921611247440E+01;
theta(2) = -0.508934679728216110E+01 + 1j*0.358882439228376881E+01;
theta(3) = -0.399337136365302569E+01 + 1j*0.600483209099604664E+01;
theta(4) = -0.226978543095856366E+01 + 1j*0.846173881758693369E+01;
theta(5) =  0.208756929753827868E+00 + 1j*0.109912615662209418E+02;
theta(6) =  0.370327340957595652E+01 + 1j*0.136563731924991884E+02;
theta(7) =  0.889777151877331107E+01 + 1j*0.166309842834712071E+02;

\end{verbatim}

The corrected coefficients have been reported also in \cite{chebCoeffs2}, where high-precision arithmetic has been employed  and they're used in this thesis.  In Figure \ref{fig:absscalar},  it's shown the error curve $|e^x - R_{14}(x)|$ in logarithmic plot with the new ones. The imperfect equioscillations are due to rounding errors.

\begin{figure}[h!]
\center
\includegraphics[width=1.0 \textwidth]{Err_corr_cheb.png}
\caption[Error in approximation of $e^x$ for $x \leq 0$ with corrected coefficients]{Absolute error in approximation of $e^x$ for $x \leq 0$ with corrected coefficients.}
\label{fig:absscalar}
\end{figure}

Here are the corrected coefficients:

\begin{verbatim}

alpha0 = 1.8321743782540412751 * 1e-14;
alpha(1) = -7.1542880635890672853 * 1e-5 + 1j*1.4361043349541300111 * 1e-4;
alpha(2) =  +9.4390253107361688779* 1e-3  - 1j*1.7184791958483017511 * 1e-2;
alpha(3) = -3.7636003878226968717 *1e-1 + 1j*3.3518347029450104214 * 1e-1;
alpha(4) = -2.3498232091082701191* 1e1 - 1j*5.8083591297142074004 * 1e0;
alpha(5) = +4.6933274488831293047 * 1e1 + 1j*4.5643649768827760791 * 1e1;
alpha(6) = -2.7875161940145646468 * 1e1 - 1j*1.0214733999056451434 * 1e2;
alpha(7) = +4.8071120988325088907 * 1e0 - 1j*1.3209793837428723881 * 1e0;



theta(1) = - 8.8977731864688888199 * 1e0 + 1j*1.6630982619902085304 * 1e1;
theta(2) = - 3.7032750494234480603 * 1e0 + 1j*1.3656371871483268171 * 1e1;
theta(3) = - 0.2087586382501301251 * 1e0 + 1j*1.0991260561901260913 * 1e1;
theta(4) = + 3.9933697105785685194 * 1e0 + 1j*6.0048316422350373178 * 1e0;
theta(5) = + 5.0893450605806245066 * 1e0 + 1j*3.5888240290270065102 * 1e0;
theta(6) = + 5.6231425727459771248 * 1e0 + 1j*1.1940690463439669766 * 1e0;
theta(7) = + 2.2697838292311127097 * 1e0 + 1j*8.4617379730402214019 * 1e0;
\end{verbatim}


%\begin{center}
%$
%\begin{array}{lcc}
%\toprule
%\label{table:chebcoeffs}
% \text{Coefficient} & \text{Real part} & \text{Imaginary part} \\
%\midrule
%\theta_1 
%& - 8.897 773 186 468 888 819 9 \cdot 10^0 & 1.663 098 261 990 208 530 4 \cdot 10^1 \\
%\theta_2
%& -3.703 275 049 423 448 060 3 \cdot 10^0 & +1.365 637 187 148 326 817 1 \cdot 10^1 \\
%\theta_3
%& -0.208 758 638 250 130 125 1  \cdot 10^0 & +1.099 126 056 190 126 091 3 \cdot 10^1 \\
%\theta_4
%& +3.993 369 710 578 568 519 4 \cdot 10^0 & +6.004 831 642 235 037 317 8 \cdot 10^0 \\
%\theta_5
%& +5.089 345 060 580 624 506 6  \cdot 10^0 & +3.588 824 029 027 006 510 2  \cdot 10^0 \\
%\theta_6
%& +5.623 142 572 745 977 124 8 \cdot 10^0 & +1.194 069 046 343 966 976 6 \cdot 10^0 \\
%\theta_7
%& +2.269 783 829 231 112 709 7 \cdot 10^0 & +8.461 737 973 040 221 401 9 \cdot 10^0	 \\
%\midrule
%\alpha_0
%& +1.832 174 378 254 041 275 1  \cdot 10^{-14} & 0 \\
%\alpha_1
%& -7.154 288 063 589 067 285 3 \cdot 10^{-5} & +1.436 104 334 954 130 011 1 \cdot 10^{-4} \\
%\alpha_2
%& +9.439 025 310 736 168 877 9  \cdot 10^{-3} & - 1.718 479 195 848 301 751 1 \cdot 10^{-2} \\
%\alpha_3
%& - 3.763 600 387 822 696 871 7 \cdot 10^{-1} & +3.351 834 702 945 010 421 4 \cdot 10^{-1} \\
%\alpha_4
%& - 2.349 823 209 108 270 119 1 \cdot 10^1 & - 5.808 359 129 714 207 400 4 \cdot 10^0 \\
%\alpha_5
%& +4.693 327 448 883 129 304 7  \cdot 10^1 & +4.564 364 976 882 776 079 1 \cdot 10^1 \\
%\alpha_6
%& - 2.787 516 194 014 564 646 8 \cdot 10^1 & -1.021 473 399 905 645 143 4 \cdot 10^2 \\
%\alpha_7
%& +4.807 112 098 832 508 890 7\cdot 10^0 & -1.320 979 383 742 872 388 1 \cdot 10^0 \\

  
  
%\bottomrule
%\end{array}
%$
%\captionof{table}{Corrected coefficients}
%\end{center}


Note that it is reasonable that $\alpha_0$ is the smallest one, since it must be $e^{x} \rightarrow 0$ as $x \rightarrow -\infty$. Nevertheless, we already know that the absolute error with Chebychev Rational Approximation Method (abbreviated as CRAM) is of order $10^{-14}$. We could of course increase its precision by increasing of $2$ the terms in the summation \eqref{eqn:CRAM} in order to achieve the double precision accuracy.


\subsection{Accuracy and relative error}

The number of correct significant digits is connected with the \emph{relative error} $$\text{E}_{\text{rel}}(x) = \frac{|\exp(x)- R_{\nu}(x)|}{|\exp(x)|}$$

The relative error graph will be dramatically different from the one with the absolute error: consider, for instance, $x=-100$. $\exp(-100)$ can still be represented in machine precision (it does not underflow) and has $43$ zeros before the first significant digits, and since the approximation $R_{\nu}(-100)$ has $14$  zeros before the first significant digits, it is $10^{29}$ the real value. Therefore the relative error is of order $10^{29}$. This situation is illustrated in Figure \ref{fig:errrelCheb}.



\begin{figure}[h!]
\center
\includegraphics[width=1.1 \textwidth]{Err_rel_cheb.png}
\caption[Relative error in approximation of $e^x$ for $x \leq 0$]{Relative error in approximation of $e^x$ for $x \leq 0$.}
\label{fig:errrelCheb}
\end{figure}





\section{Extension to matrix case}
The extension to matrix case is allowed for symmetric and negative  semidefinite real matrices and so in the following $\sigma(A)$ has to be assumed in $[\alpha,0]$, $\alpha <0$. This is not restrictive because if $\sigma(A) \subset [\alpha,\beta]$, then we can consider the shifted matrix $\tilde{A} = A - \beta I$, whose spectrum is contained in  $[\alpha,0]$. Therefore $e^A = e^{\tilde{A}} e^{\beta}$, but the issue of shifting is that the bound \eqref{eqn:chebound} becomes not significant anymore because $e^{\beta}$ becomes quickly large and magnifies the error. Since the matrix $A$ is symmetric, we have the eigendecomposition $$A = Q D Q^{-1}$$ where $Q \in \mathbb{R}^{n \times n}$ is orthogonal. Then, from the  definition of matrix exponential, we have $$e^{A} = Q e^{D} Q^{-1}$$ and for $i,j =1,\ldots,n$: $\bigl(e^{D}\bigr)_{ij} = e^{\lambda_i}$ if $i=j$ and $0$ otherwise,  where $\lambda_i \in \mathbb{R}^{-}_0$ are the eigenvalues of $A$. Substituting formally the symmetric matrix $A$ in place of $x$ in $R_{\nu}(x)$:
\begin{multline}
\alpha_0 I + \sum_{j=1}^{\nu} \alpha_j (A- \theta_i I)^{-1}  = \alpha_0 I  + \sum_{j=1}^{\nu} \alpha_j (QDQ^{-1} - \theta_j I)^{-1}  = \alpha_0 I + \sum_{j=1}^\nu \alpha_j Q(D - \theta_j I)^{-1}Q^{-1} = \\ Q \Bigl[  \alpha_0 I + \sum_{j=1}^\nu  (D - \theta_j I)^{-1}  \Bigr]Q^{-1}  =   Q  \Bigr[ \text{CRAM}(D) \Bigl] Q^{-1} \approx e^{A}
\end{multline}


where $\text{CRAM}(D)$ stands for the ``Chebychev Rational Approximation Method'' for the approximation of $e^{D}$.  Therefore 
\begin{multline}
\label{eqn:stimaCRAM}
\norm{e^{A} - \text{CRAM}(A)} =  \norm{Q e^{D} Q^{-1} - Q \text{CRAM}(D)Q^{-1} } = \norm{Q (e^{D} - \text{CRAM}(D))Q^{-1}}  \\ \leq \text{cond}(Q) \cdot \norm{e^{D} - \text{CRAM}(D)}
\end{multline}

As $D$ is diagonal, we have that the last term in the inequality above is $$\max_{\lambda_j \in \sigma(A) } | e^{\lambda_j} - \text{CRAM}(\lambda_j)| \approx 10^{-\nu}$$

where we used the scalar bound \eqref{eqn:chebound}. Since $Q$ is orthogonal, $\text{cond}(Q)=\norm{Q}_2 \norm{Q^{-1}}_2=1$. Hence,  the following estimate holds: \[ \norm{e^{A} - \text{CRAM}(A)} \approx 10^{-\nu} \]



%! Ho anche l'errore dei sistemi lineari...



We can therefore consider the action of the exponential on a vector $v$ as



\begin{equation}
\label{eqn:CRAM}e^A v \approx R_{\nu}(A) v = \alpha_0 v + \sum_{j=1}^{\nu} \alpha_j (A - \theta_j I)^{-1} v
\end{equation}

 
 
and we can extend the bound $\eqref{eqn:chebound}$ to the matrix case \[ \norm{e^A v - R_{\nu}(A)v } \approx 10^{-\nu} \]
Since the poles in the expansion come in complex conjugates, we can reduce the work by a factor of two and write:
 
\begin{equation}
\label{eqn:CRAMtrick}
e^A v \approx  \alpha_0 v +  2 \text{Re} \Bigl( \sum_{j=1}^{\frac{\nu}{2}}   \alpha_j (A - \theta_j I)^{-1} v \Bigr)
\end{equation}


The good point of this method is that it's embarrassingly parallel, as all the shifted linear systems are independent of each other and the crucial point is to solve them efficiently. Since matrices that arise from PDE semi-discretizations are sparse and large, often memory and computational issues require sparse  iterative solvers to be used. 


\section{Hermitian matrices}

Since eigenvalues of complex Hermitian matrices are real, the method described above applies also to this class of matrices, whenever the spectrum is contained in $(-\infty,0]$. Indeed, the analysis done in the previous section can be repeated, with the minor modification that now $$A = UDU^{\dagger}$$ where $U$ is a unitary matrix. Unfortunately, the fact that the entries of the matrix are complex (except the main diagonal) doesn't allow to reduce the summation terms as in \eqref{eqn:CRAMtrick} and hence $14$ linear systems has to be solved in order to compute $R_{14}(A)v$. Note that if the matrix $A$ was not Hermitian (resp. symmetric), the change of basis matrix $Q$ may not be unitary (resp. orthogonal), and therefore the estimate \eqref{eqn:stimaCRAM} depends strongly on $\text{cond}(Q)$, which in principle could be really large for a general matrix, leading to poor accuracy of this method.



\subsection{Stability}

Every exponential time-stepping method relies on repeated applications of $R_{\nu} (\tau A)$ (where $\tau$ is the time-step size), so 
in order to avoid a bad error propagation we want the method to be stable. Using the error bound \eqref{eqn:chebound} and the fact that $\lambda \leq 0$, we have:

\[
\max_{\lambda \in \sigma(A)} | R_{\nu}(\tau \lambda) | =\max_{\lambda \in \sigma(A) } |e^{\tau \lambda} + \bigl( R_{\nu}(\tau \lambda) - e^{\tau \lambda} \bigr) |  \leq 1 + 10^{-\nu}
\]

Then it follows that 

\[
\max_{\lambda \in \sigma(A) } |\bigl( R_{\nu}(\tau \lambda) \bigr)^n| \leq \max_{\lambda \in \sigma(A) } |\bigl( R_{\nu}(\tau \lambda) \bigr)|^n \leq (1+10^{-\nu})^n \leq e^{10^{- \nu} n}
\]

Hence, the method is stable as long as we have $10^{- \nu} n < 1$. Notice that for the choice $\nu = 14$, we would break stability if $n>10^{14}$, which in practice is never satisfied (it means we are doing more than $10^{14}$ iterations). Also, that holds independently of the time-step $\tau$ and eigenvalue $\lambda$.

\section{Spectrum and loss of accuracy}

The accuracy of the method strongly depends on the fact that the spectrum of the matrix $A$ is contained in $(-\infty, 0]$. Let $A$ the matrix of size $n=70$ stemming from the finite difference discretization of the Laplacian operator described in the first chapter \eqref{mat:lapl}
 and consider the shift $A + \lambda I$, with $\lambda \in \mathbb{Z}$. This shifted matrix is clearly symmetric, and the eigenvalues of $A$ are shifted to left or right, depending if $\lambda$ is negative or positive, respectively. When this parameter is so large that one of the eigenvalues becomes strictly positive, we loose accuracy, as expected from the scalar theory, since the matrix is no more negative semidefinite. Figures \ref{fig:abserrshift} and \ref{fig:eigshift} illustrate this fact: in the first semi-logarithmic plot there is the absolute error in the computation of the action $e^{A+\lambda I} v$, with $v$ a normally random vector, with $R_\nu(A + \lambda I)$ as a function of the shift parameter $\lambda$, which runs from -200 to 50. We  loose accuracy when the spectrum is no more contained in the left half plane and hence as we increase $\lambda$ we obtain catastrophically wrong results.  Figure 3.4 shows the largest eigenvalue  (not the spectral radius) of $A + \lambda I$, again as a function of $\lambda$. The black cross are those that give poor accuracy. The reference solution was computed by \verb|expm(A)*v|.




\begin{figure}[h!]
\center
\includegraphics[width=1.0 \textwidth]{apprx_error_CRAM.png}
\caption[Approximation error and shifting]{Absolute error in $2$-norm in the approximation of $e^{A+\lambda I}v$ as function of $\lambda$.}
\label{fig:abserrshift}
\end{figure}


\begin{figure}[h!]
\center
\includegraphics[width=1.0 \textwidth]{largest_eig.png}
\caption[Eigenvalues of $A+ \lambda I$]{Largest eigenvalue of $A+ \lambda I$ for different values of $\lambda$.}
\label{fig:eigshift}
\end{figure}


\newpage

\section{CRAM and \emph{expm}}
The function \verb|expm| is the most (mis-)used MatLab command to compute the matrix exponential and can be applied to \emph{all} kind of matrices. It is based on the \emph{scaling and squaring} method idea, which is $e^A \approx r_m\Bigl( \frac{A}{2^s} \Bigr)^{2^s}$ where $r_m(x)$ is the $[m/m]$ Padè approximant to $e^x$, and $m,s$ are parameters to be chosen. Its detailed description is in \cite{Higham}. It is interesting to investigate its computational time against the CRAM method for the matrix $A=\text{diag}\Bigl(\log(d)\Bigr)$ where $d$ is a vector with equispaced entries in $[0.2,0.99]$, so that the entries of matrix are all negative, increasing iteratively its size. The matrix is stored in sparse format and the linear systems are solved using the direct sparse solvers available in the MatLab \emph{backslash} \verb|\|. This has been performed on an Intel Core i5-6200U CPU with 4GB of RAM using MatLab R2020b. $v$ is a compatible vector of ones, so that $e^A v = d$ is the exact solution.



\begin{figure}[h!]
\center
\includegraphics[width=1.0 \textwidth]{CRAMvsExpmdiagonal.png}
\caption[Computational time (in seconds) for the computation of $e^A v$]{Computational time (in seconds) for the computation of $e^A v$.}
\end{figure}

In the next Figure \eqref{fig:workprec} we show the \emph{working precision diagram}, i.e. the relative error $E_\text{rel} = \frac{\norm{e^{A}v- d} }{\norm{d}}$, where $e^{A}v$ has been computed with 
\begin{itemize}
\item $\text{CRAM}$ 
\item \verb|expm|
\item \verb|expm(full())| (passing as argument a ``full'' matrix reduce the computational cost)
\end{itemize}
plotted against the computational time.


\begin{figure}[h!]
\center
\includegraphics[width=1.0 \textwidth]{WorkingPrecisionCRAMvsExpm.png}
\caption[Working precision diagram]{Computational time against relative error.}
\label{fig:workprec}
\end{figure}


