
Recall that the ``rational'' methods to approximate $e^{A}v$ can be applied to two different class of problems, depending if $\sigma(A) \subset i \mathbb{R}$ or $\sigma(A) \subset (-\infty,0]$ and $A$ symmetric. The $\mu$-mode method only cares about the structure of the matrix itself, and can be applied independently of the spectrum of the differential operator. Also, its memory requirement is crucial in performance. Therefore the comparisons are: 
\begin{itemize}
\item  $\mu$-mode versus \emph{Chebychev rational expansion} (\text{CRAM})\\
\item $\mu$-mode versus \emph{REXI}
\end{itemize}


It's clear that the $\mu$-mode method uses deeply the Kronecker-sum structure of the matrix of the problem. It is not parallelizable as the other methods, but it uses the multi-threading of level 3 BLAS operations% (\cite{BLASinfo})
. As seen in \cite{mumode}, the lower memory requirement of the $\mu$-mode method is in any case faster that the currents state of art competitors.
It is not clear if for large dimensions it's better to use a ``parallel'' method or the structure of the matrix. 

\section{$\mu$-mode versus CRAM}


\subsection{3d Laplacian}

Let $$A =I \otimes I \otimes A_1 +  I \otimes A_2 \otimes I + A_3 \otimes I \otimes I$$

where each square $A_i$ is the matrix \eqref{mat:lapl}, all with the same dimension $m$. This corresponds to the discretization with finite differences of the Laplace operator in three dimensions. Therefore, the size of $A$ is $m^3$. We let $m$ ranges from $10$ to $50$, and compute the action of $e^A$ on some compatible random vector $v$. The linear systems in CRAM can be solved with the \verb|tfqmr| MatLab routine (a \emph{transpose free} variant of the QMR algorithm) in parallel using \verb|parfor| or with the Shifted COCR method. We can observe in table \ref{table:timescram} and in Figure \ref{fig:cramsvsmumode} that with the  we have a significative speed up with respect to the \verb|tfqmr| alternative. This is because in MatLab, linear algebra and numerical functions are multithreaded, and therefore we don't observe perfect scalability. In any case, the $\mu$-mode method is significantly faster than the other ones. The experiments have been performed on the CentOS Linux 7 cluster at Università degli Studi di Verona. 


\begin{center}
$
\begin{array}{lccccccccc}

\toprule
\label{table:timescram}

 \text{Method} & $m=10$ & $m=15$ & $m=20$ & $m=25$  & $m=30$ & $m=35$ & $m=40$ & $m=45$ & $m=50$  \\
\midrule
\mu\text{-mode} &  0.001939 & 0.002158  & 0.002168 & 0.001958 & 0.002374 & 0.002473  & 0.003466 & 0.003450 & 0.003283 \\
\text{CRAM(tfqmr)} &      0.4829 &     0.4847  &     0.5453 &     0.5530  &     0.6674 &     0.9333 &    1.1120 &     1.4468 & 1.7287 \\
\text{CRAM(COCR)} &    0.04365 &     0.04680  &     0.09003 &     0.1287  &     0.2016 &     0.2950 &    0.3602 &     0.5379 & 0.6793 \\
\bottomrule
\end{array}
$
\captionof{table}{Computational time for $e^A v $ for increasing $m$}
\end{center}




\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{CRAMsvsMumode.png}
\caption[CRAM vs. $\mu-$mode]{Computational time (in seconds) }
\label{fig:cramsvsmumode}
\end{figure}


\subsection{Heat equation}
As a test problem, consider the \emph{d--dimensional Heat equation with source term} 

\[
\begin{cases}
\partial_t U(x,t) - \Delta U(x,t) = \frac{1}{1+U(x,t)^2} + \Phi(x,t) \\
U(x,0)= \prod_{i=1}^{d} x_i(1-x_i), \quad x \in [0,1]^d
\end{cases}
\]
for $t \in [0,1]$, coupled with Dirichlet homogenenous boundary conditions. The function $\Phi$ is computed via the manufactured solution method, i.e. it is chosen in such a way that the analytical solution is $U(x,t) = e^t  \prod_{i=1}^{d} x_i(1-x_i)$. Using this ansatz for $d=2$, we can just plug $U(x,t)$ into the equation and obtain 
\[ 
\Phi(x,t) = e^{t} xy(1-x)(1-y) - 2e^{t}(x^2+y^2 - x - y) - \frac{1}{1+(e^{t} xy(1-x)(1-y) )^2}
\]

 After the spatial discretization with centered, second order, finite differences one obtains a large-scale stiff system of ODEs 
 
\[
\label{eqn:Heateq}
 \partial_t U_h  = M U_h(t) + g(t,U_h(t)) 
\]
 
 where the matrix $M$ is \eqref{mat:lapl} (which is in Kronecker form) and $\Phi(x,t) \rightarrow \Tilde{\Phi}(t)$ will be just a function of time inside the function $g(t,U_h(t))$.  For $d=2$ we can see in Figure \eqref{fig:inisurface} the inital surface.
 
 
\begin{figure}[h]
\center
\includegraphics[width=0.7 \textwidth]{InitialSurfaceHeateqColorbar.png}
\caption[Initial surface]{Surface at time $t=0$.}
\label{fig:inisurface}
\end{figure}
 
 
The time integration is performed, according to the authors in \cite{mumode}, using the Lawson2a exponential integrator with constant time step $k=10^{-2}$

\[ 
\begin{cases}
\label{eqn:lawson2a}
\xi_1 = U_h^n, \\
\xi_2 = U_h^n+ \frac{1}{2} k \exp(\frac{1}{2}k M) (g(t_n, \xi_1) + M U_h^n), \\
U_h^{n+1} = U_h^n + k \exp(\frac{1}{2}kM) (g(t_n + \frac{1}{2}k, \xi_2) + MU_h^n)
\end{cases}
\]

where the matrix exponentials are computed using the two different methods. The solution surface $\{ (x,y,U(x,y)) \}$ at $T=1$ is depicted in Figure \eqref{fig:finsurfce}. 
\begin{figure}[h]
\center
\includegraphics[width=0.7 \textwidth]{HeateqColorbarT1.png}
\caption[Surface at $T=1$]{Surface at time $T=1$. The surface increases in time.}
\label{fig:finsurfce}
\end{figure}

The correctness of the implementation is justified by the correct \emph{classical} order of convergence observed in Figure \eqref{fig:convord}, computing the $2-$norm of the error with respect to the analytical solution, increasing iteratively the number of timesteps.

\begin{figure}[h]
\center
\includegraphics[width=0.75 \textwidth]{errorHeat.png}
\caption[Order of convergence]{Classical order of convergence for \eqref{eqn:lawson2a}}
\label{fig:convord}
\end{figure}

 While the CRAM like implementation is straightforward, the solution with the $\mu-$mode is different and be outlined as follows:

\begin{algorithm}
\caption{$\mu-$mode implementation}
\begin{algorithmic}
\State Store matrices $A_x$, $A_y$ \%1D [1 -2 1] stencil
\State [X,Y] = ndgrid(x,y)  \%compute nd mesh
\State Store initial data $U_0(X,Y)$
\State Set time step $k$, final time $T$, and number of timesteps $\text{tsteps}$
\State Define $g(t,U) = \frac{1}{1+U^2} + \Tilde{\Phi}(t)$
\State $E\{1\}=A_x$, $E\{2\}=A_y$ \%$\mu-$mode matrices
\State $U=U_0$
\For { $n=1,\ldots, \text{tsteps}$ }
\State Precompute $MUn =  $actkron(U,E)
\State $\xi_1  = U$
\State $\xi_2 = U + \frac{k}{2} \cdot$mmpnd $(g(t,\xi_1) +  MUn,E)$
\State $U = U + k \cdot $ mmpnd$( g(t+\frac{k}{2},\xi_2)+ MUn,E)$
\EndFor
\end{algorithmic}
\end{algorithm}

The computational times for the integration of \eqref{eqn:Heateq} up to time $T=1$ for an increasing number of space points are reported in the next Figure \eqref{fig:CRAMmumoheat}. As expected from the previous table, the $\mu-$mode method is clearly faster.

\begin{figure}[h]
\center
\includegraphics[width=1.0 \textwidth]{CRAMvsMumodeHeatTimes.png}
\caption[Integration of Heat equations]{Computational time (in seconds) for the integration of \eqref{eqn:Heateq}.}
\label{fig:CRAMmumoheat}
\end{figure}
 

\section{$\mu$-mode versus REXI}

The discretization with second order finite differences of $i \Delta$ with periodic boundary conditions in $2$-dimensions is 

\[
A = I \otimes A_1 + A_2 \otimes I
\]

where each $A_i$ is the matrix \eqref{mat:laplperiodic} with size $m$. The size of $A$ is therefore $m^2$. We let $m$ ranges from $10$ to $30$, and compute the action on a compatible vector using REXI. In particular, we are in case \eqref{eqn:fcase}, where we have to solve a linear system for summation term. For every $m$, we are solve a number of linear systems which is greater by (cfr. inequality \eqref{eqn:ineqREXImatr}) $\ceil[\Bigg]{\frac{\rho}{h} + m_0}$ (where $h = 0.5$,$m_0=11$ and $\rho$ is the spectral radius of the matrix). Since increasing the size of the matrix also \emph{enlarges} the spectrum of the matrix $A$, more summation terms are required, i.e. more linear systems. We can observe in Figure \eqref{fig:REXIvsmumode} how large are computational times for REXI in this case: the linear solver cost dominates the overall cost of the algorithm and it rapidly becomes too expensive.

\begin{figure}[h!]
\center
\includegraphics[width=0.9 \textwidth]{REXIvsMumode.png}
\caption[REXI vs. $\mu$-mode]{Computational time (in seconds).}
\label{fig:REXIvsmumode}
\end{figure}
 
